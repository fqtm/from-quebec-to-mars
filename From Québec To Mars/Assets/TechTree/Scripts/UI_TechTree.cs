﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_TechTree : MonoBehaviour
{
     public Image check;
     private Technologies _technologies;
     public Technologies.TechType tech;

     private void Start()
     {
          _technologies = GameObject.FindGameObjectWithTag("Tech_tree").GetComponent<PlayerTechTree>().getPlayersTech();
     }

     public void Enable()
     {
         check.enabled = true;    
          
     }
}
