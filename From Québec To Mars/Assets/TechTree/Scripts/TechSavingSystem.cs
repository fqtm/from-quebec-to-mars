using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class TechSavingSystem
{
    public static void Save(Technologies technologies)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/tech.txt";
        FileStream stream = new FileStream(path, FileMode.Create);

        TechData data = new TechData(technologies);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static TechData Load()
    {
        string path = Application.persistentDataPath + "/tech.txt";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            TechData data = formatter.Deserialize(stream) as TechData;
            stream.Close();
            return data;
        }

        Debug.LogError("File not found: " + path);
        return null;
    }
}
