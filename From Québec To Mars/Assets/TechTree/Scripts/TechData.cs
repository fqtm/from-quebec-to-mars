using System;
using System.Linq;


[Serializable]
public class TechData
{
    public int[] UnlockedTech;
    public int TechPoint;

    public TechData(Technologies technologies)
    {
        UnlockedTech = technologies.unlockedTechTypeList.Select(type => (int) type).ToArray();
        TechPoint = technologies.TechPoint;
    }
}
