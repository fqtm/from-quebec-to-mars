﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Technologies
{
    public event EventHandler<OnTechUnlockedEventArgrs> OnTechUnlocked;


    public class OnTechUnlockedEventArgrs : EventArgs
    {
        public TechType TechType;
    }

    public enum TechType
    {
        None,
        Basic_Build,
        Chest_Conveyor,
        Weapon,
        Tesla_Wall,
        Basic_Build2,
        Speed,
        LifeUpgrade1,
        LifeUpgrade2,
        Weapon2,
        Tesla_Wall2,
        Speed2,
        Poutine,
        
    }

    public List<TechType> unlockedTechTypeList;
    public int TechPoint;

    public Technologies()
    {
        unlockedTechTypeList = new List<TechType>();
        TechPoint = 2;
    }

    private void UnlockTech(TechType techType)
    {
        if (!IsTechUnlocked(techType))
        {
            unlockedTechTypeList.Add(techType);
            OnTechUnlocked?.Invoke(this, new OnTechUnlockedEventArgrs {TechType = techType});
            AddTechpoint(-1);
            Debug.Log(TechPoint);
        }
    }

    public bool IsTechUnlocked(TechType techType)
    {
        return unlockedTechTypeList.Contains(techType);
    }

    public List<TechType> GetTechRequirement(TechType techType)
    {
        List<TechType> list = new List<TechType>();
        switch (techType)
        {
            case TechType.Chest_Conveyor:
                list.Add(TechType.Basic_Build);
                break;
            case TechType.Weapon:
                list.Add(TechType.Basic_Build);
                break;
            case TechType.Tesla_Wall:
                list.Add(TechType.Weapon);
                break;
            case TechType.Basic_Build2:
                list.Add(TechType.Chest_Conveyor);
                break;
            case TechType.Speed:
                list.Add(TechType.Chest_Conveyor);
                break;
            case TechType.LifeUpgrade1:
                list.Add(TechType.Tesla_Wall);
                list.Add(TechType.Speed);
                break;
            case TechType.LifeUpgrade2:
                list.Add(TechType.LifeUpgrade1);
                break;
            case TechType.Weapon2:
                list.Add(TechType.Tesla_Wall);
                break;
            case TechType.Tesla_Wall2:
                list.Add(TechType.Weapon2);
                break;
            case TechType.Speed2:
                list.Add(TechType.Speed);
                list.Add(TechType.Basic_Build2);
                break;
            case TechType.Poutine:
                list.Add(TechType.Tesla_Wall2);
                list.Add(TechType.LifeUpgrade2);
                list.Add(TechType.Basic_Build2);
                break;
            default:
                list.Add(TechType.None);
                break;
        }

        return list;
    }

    public bool CanUnlockBuild(TechType techType)
    {
        List<TechType> techRequirement = GetTechRequirement(techType);
        bool r = true;
        int i = 0;
        if (TechPoint <= 0)
        {
            return false;
        }
        while (i < techRequirement.Count && r)
        {
            if (!IsTechUnlocked(techRequirement[i]))
            {
                r = false;
            }

            if (techRequirement[i] == TechType.None)
            {
                r = true;
            }

            i++;
        }

        return r;
    }

    public bool TryUnlockTech(TechType techType)
    {
        if (CanUnlockBuild(techType))
        {
            UnlockTech(techType);
            return true;
        }

        return false;
    }

    public void AddTechpoint(int i)
    {
        TechPoint += i;
    }
    
    
}