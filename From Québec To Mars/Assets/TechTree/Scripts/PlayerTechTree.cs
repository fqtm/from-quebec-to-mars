﻿using UnityEngine;

public class PlayerTechTree : MonoBehaviour
{
    private Technologies _playersTech;
    
    private void Awake()
    {
        _playersTech = new Technologies();
    }

    public Technologies getPlayersTech()
    {
        return _playersTech;
    }
}
