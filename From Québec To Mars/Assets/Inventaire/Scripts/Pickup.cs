﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Pickup : NetworkBehaviour
{
    public GameObject itemButton;
    [SerializeField] private uint id;

    public uint GetId()
    {
        return id;
    }
}