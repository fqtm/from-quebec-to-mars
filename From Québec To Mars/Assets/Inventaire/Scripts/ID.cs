﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Serialization;


public class ID : MonoBehaviour
{
    public GameObject[] resources;
    public GameObject[] buildings;
    public CraftingRecipe[] recipes;
}
