﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using Mirror;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class DragNDrop : NetworkBehaviour , IPointerDownHandler , IBeginDragHandler , IEndDragHandler  , IDragHandler
{
    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    [NonSerialized] public uint DnD_Id;

    private void Awake()
    {
        canvas = GetComponentInParent<Canvas>();
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        DnD_Id = Utils.GetID(gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        ISlotable slot = GetComponentInParent<ISlotable>();
        if (eventData.button == PointerEventData.InputButton.Right && slot.GetNb() > 1)
        {
            GameObject button = Instantiate(Utils.GetResourceButton(DnD_Id), transform.parent, false);
            int Base = slot.GetNb();
            int x = Base % 2 == 0 ? Base / 2 : Base / 2 + 1;
            GetComponentInChildren<Text>().text = x > 1 ? x.ToString() : "";
            button.GetComponentInChildren<Text>().text = (Base / 2) > 1 ? (Base / 2).ToString() : "";
        }
        
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData evenData)
    {
        rectTransform.anchoredPosition += evenData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        canvas = GetComponentInParent<Canvas>();
        if (eventData.button == PointerEventData.InputButton.Right && GetComponentInParent<ISlotable>().GetNb() > 1)
        {
            ISlotable slotable = GetComponentInParent<ISlotable>() ;
            if (slotable is Slot)
            {
                if (transform.parent.childCount > 2)
                {
                    Destroy(transform.parent.GetChild(2).gameObject);
                    slotable.UpdateSlot(slotable.GetNb());
                }
            }
            else
            {
                if (transform.parent.childCount > 1)
                {
                    Destroy(transform.parent.GetChild(1).gameObject);
                    slotable.UpdateSlot(slotable.GetNb());
                }
            }
        }
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }
}
