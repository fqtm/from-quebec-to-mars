
public interface ISlotable
{
    void Clear();
    void UpdateSlot(int nb);

    int GetNb();

    void IncreaseNb(int i);
}
