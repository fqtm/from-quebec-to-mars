﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ItemSlot : MonoBehaviour , IDropHandler
{

    private void Start()
    {
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        GameObject button = eventData.pointerDrag;
        Debug.Log("OnDrop");
        if (button != null)
        {
            ISlotable PrevSlot = button.GetComponentInParent<ISlotable>();
            ISlotable CurrentSlot = GetComponent<ISlotable>();
            if (eventData.button == PointerEventData.InputButton.Left || PrevSlot.GetNb() == 1)
            {
                if (PrevSlot != CurrentSlot)
                {
                    if (CurrentSlot.GetNb() == 0)
                    {
                        button.transform.SetParent(transform, false);
                        CurrentSlot.UpdateSlot(PrevSlot.GetNb());
                        PrevSlot.Clear();
                    }
                    else if (button.GetComponent<DragNDrop>().DnD_Id == GetComponentInChildren<DragNDrop>().DnD_Id)
                    {
                        if (PrevSlot.GetNb() + CurrentSlot.GetNb() <= Utils.MaxStack)
                        {
                            CurrentSlot.IncreaseNb(PrevSlot.GetNb());
                            PrevSlot.Clear();
                            Destroy(eventData.pointerDrag.gameObject);
                        }
                        else
                        {
                            int diff = Utils.MaxStack - CurrentSlot.GetNb();
                            CurrentSlot.IncreaseNb(diff);
                            PrevSlot.IncreaseNb(-diff);
                        }
                    }
                }
            }
            else if (eventData.button == PointerEventData.InputButton.Right)
            {
                int Base = PrevSlot.GetNb();
                int nb = Base % 2 == 0 ? Base / 2 : Base / 2 + 1;
                if (PrevSlot != CurrentSlot)
                {
                    if (CurrentSlot.GetNb() == 0)
                    {
                        button.transform.SetParent(transform, false);
                        CurrentSlot.UpdateSlot(nb);
                        PrevSlot.UpdateSlot(Base - nb);
                    }
                    else if (button.GetComponent<DragNDrop>().DnD_Id == GetComponentInChildren<DragNDrop>().DnD_Id)
                    {
                        if (nb + CurrentSlot.GetNb() <= Utils.MaxStack)
                        {
                            CurrentSlot.UpdateSlot(nb + CurrentSlot.GetNb());
                            Destroy(eventData.pointerDrag.gameObject);
                            PrevSlot.UpdateSlot(Base - nb);
                        }
                        else
                        {
                            int diff = Utils.MaxStack - CurrentSlot.GetNb();
                            CurrentSlot.UpdateSlot(Utils.MaxStack);
                            PrevSlot.UpdateSlot(Base - diff);
                        }
                    }
                }
            }
        }
    }
}
