﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Player.Scripts;
using UnityEngine.UI;

public class Slot : MonoBehaviour , ISlotable
{
    private Inventory _inventory;
    public int i;
    private GameObject player;
    private NetworkIdentity NI;
    private int nb;
    private GameObject CO;
    [NonSerialized] public Text stack;
    public Button removeButton;

    private void Start()
    {
        player = transform.root.gameObject;
        NI = player.GetComponent<NetworkIdentity>();
        if (NI.isLocalPlayer)
        {
            nb = 0;
            CO = null;
            
            _inventory = player.GetComponent<Inventory>();


            foreach (Transform child in transform)
            {
                if (child.CompareTag("Text"))
                {
                    stack = child.GetComponent<Text>();
                    if (nb < 2)
                        stack.text = "";
                    else
                        stack.text = nb + "";
                }
            }

        }
    }

    public void Clear()
    {
        if (transform.childCount > 1)
        {
            GameObject button = transform.GetChild(1).gameObject;
            button.transform.SetParent(null);
            Destroy(button);
        }
            
            
        CO = null;
        stack = null;
        nb = 0;
        removeButton.interactable = false;
        _inventory.isFull[i] = false;
    }

    public void UpdateSlot(int nb)
    {
        if (nb == 0)
            Clear();
        else
        {
            CO = transform.GetChild(1).gameObject;
            stack = CO.GetComponentInChildren<Text>();
            this.nb = 0;
            IncreaseNb(nb);
            removeButton.interactable = true;
            _inventory.isFull[i] = true;
        }
    }

    public void DropItem()
    {
        _inventory.DropItem(i);
    }

    public void SetCO(GameObject CO)
    {
        this.CO = CO;
    }

    public GameObject GetCO()
    {
        return CO;
    }

    public int GetNb()
    {
        return nb;
    }
    
    public void IncreaseNb(int i)
    {
        nb += i;
        if (nb < 2)
            stack.text = "";
        else
            stack.text = nb + "";
        if (nb <= 0) Clear();
    }
}