﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using Random = System.Random;

public class WaweSpawner : MonoBehaviour
{
    
    public enum SpawnState
    {
        SPAWNING,
        UNDERATTACK,
        SAFE,
    }
    
    [System.Serializable]
    public class Wawe
    {
        public string name;
        public Transform[] enemy;
        public int[] count;
        public float rate;
    }

    public Wawe[] wawes;
    private int nextwawe = 0;

    public Transform[] spawnpoints;

    public float timebetweenwawes = 450f;
    public float wawecoountdown;
    private float searchcountdown = 1f;

    public SpawnState state = SpawnState.SAFE;

    private void Start()
    {
        wawecoountdown = 0f;
    }

    private void Update()
    {
        if (state == SpawnState.UNDERATTACK)
        {
            if (!EnemIsAlive())
            {
                WaweCompleted();
            }
            else
            {
                return;
            }
        }
        
        if (wawecoountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWawe(wawes[nextwawe]));
            }
        }
        else
        {
            wawecoountdown -= Time.deltaTime;
        }
    }

    IEnumerator SpawnWawe(Wawe wawe)
    {
        Debug.Log("Spawning wawe");
        state = SpawnState.SPAWNING;
        for (int i = 0; i < wawe.count.Length; i++)
        {
            for (int j = 0; j < wawe.count[i]; j++)
            {
                SpawnEnemy(wawe.enemy[i]);
                yield return new WaitForSeconds(1);
            }
        }

        state = SpawnState.UNDERATTACK;
        yield break;
    }

    void SpawnEnemy(Transform enemy)
    {
        if (!ClientScene.localPlayer.isServer) return;
        Debug.Log("Spawning Enemy : "+ enemy.name);
        Transform sp = spawnpoints[UnityEngine.Random.Range(0, spawnpoints.Length)];
        GameObject go = Instantiate(enemy, sp.transform.position, sp.transform.rotation).gameObject;
        NetworkServer.Spawn(go);
    }

    bool EnemIsAlive()
    {
        searchcountdown -= Time.deltaTime;
        if (searchcountdown <= 0f)
        {
            searchcountdown = 1f;
            if (GameObject.FindWithTag("monsteralpha") == null && GameObject.FindWithTag("monsterbeta") == null)
                return false;
        }
        return true; 
    }

    void WaweCompleted()
    {
        Debug.Log("Wawe finished");
        nextwawe = TypeOfWawe();
        state = SpawnState.SAFE;
        wawecoountdown = timebetweenwawes;
    }

    int TypeOfWawe()
    {
        int n = GameObject.FindGameObjectsWithTag("Building").Length;
        if (n > 39) return 3;
        return 10 / n;
    }
}
