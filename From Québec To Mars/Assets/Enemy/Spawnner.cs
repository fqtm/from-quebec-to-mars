﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Spawnner : NetworkBehaviour
{
    public GameObject BasicEnemy;
    // Start is called before the first frame update
    void Start()
    {
        if (!isServer) return;
        GameObject go = Instantiate(BasicEnemy);
        NetworkServer.Spawn(go);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
