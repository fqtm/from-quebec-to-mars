﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Drop : MonoBehaviour
{
    [SerializeField] private GameObject drop;

    public void Dropping()
    {
        ClientScene.localPlayer.GetComponent<BuildingNetwork>().CmdLoot(Utils.GetID(drop),transform.position);
    }
}
