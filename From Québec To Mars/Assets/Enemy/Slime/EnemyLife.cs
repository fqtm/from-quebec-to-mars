﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLife : NetworkBehaviour
{
    public int health;
    public int maxHealth;
    private bool isDestroy;
    public void TakeDamage(int amount)
    {
        if(!isServer) return;
        health -= amount;
        if (health <= 0)
        {
            gameObject.GetComponent<Drop>().Dropping();
            Destroy(gameObject);
        }
    }
}
