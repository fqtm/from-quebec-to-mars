﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using Mirror.Logging;
using Pathfinding;
using Player.Scripts;
using UnityEngine;

public class BasicEnemy : NetworkBehaviour
{
    private float nextattacktime;
    public int damage;
    public float attackrange; 
    public float range; // Range for chase the player
    public GameObject target; //Current target of the mob
    public bool TargetFound; //Bool if mob has a target
    public GameObject targetDestroyed;

    public AIPath aiPath; // Road of the mob
    
    public enum State
    {
        ChaseBuilding,
        ChasePlayer,
        Nothing
    }

    public State state;
    // Start is called before the first frame update
    void Start()
    {
        targetDestroyed = null;
        if (!TargetFound)
        {
            
            target = FindClosestBuilding();
            if (target != null)
            {
                state = State.ChaseBuilding;
                GetComponent<AIDestinationSetter>().target = target.transform;
                TargetFound = true;
            }
            else
            {
                state = State.Nothing;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Select the target
        if (target == null)
        {
            TargetFound = false;
            target = null;
        }
        switch (state)
        {
            case State.ChaseBuilding:
                GameObject player = FindClosestPlayer();
                if (CanChasePlayer(player))
                {
                    TargetFound = true;
                    state = State.ChasePlayer;
                    target = player;
                    GetComponent<AIDestinationSetter>().target = target.transform;
                }
                else if (CanAttack())
                {
                    if (target!=null) target.GetComponent<BuildingLife>().TakeDamage(damage);
                    if (target!=null)
                    {
                        if (target.GetComponent<BuildingLife>().GetDestroy())
                        {
                            AstarPath.active.UpdateGraphs (target.GetComponent<BoxCollider2D>().bounds);
                            targetDestroyed = target;
                            TargetLostBuildingB(target);
                            targetDestroyed.GetComponent<Building>().RemoveBuilding();
                        }
                    }
                    
                }
                break;
            
            case State.ChasePlayer:
                if (!CanChasePlayer(target))
                {
                    target = FindClosestBuilding();
                    if (target != null)
                    {
                        state = State.ChaseBuilding;
                        GetComponent<AIDestinationSetter>().target = target.transform;
                    }
                    else
                    {
                        GetComponent<AIDestinationSetter>().target = null;
                        TargetFound = false;
                        state = State.Nothing;
                    }
                    
                }
                else if (CanAttack())
                {
                    target.GetComponent<PlayerNetwork>().TakeDamage(damage);
                    if (target.GetComponent<PlayerNetwork>().currentHealthPlayer <= 0 )
                    {
                        GameObject t = target;
                        TargetLostPlayer(target);
                        t.GetComponent<PlayerNetwork>().Respawn();
                    }
                }
                break;
            
            case State.Nothing:
                target = FindClosestPlayer();
                if (CanChasePlayer(target))
                {
                    state = State.ChasePlayer;
                    GetComponent<AIDestinationSetter>().target = target.transform;
                    TargetFound = true;
                }
                else
                {
                    target = FindClosestBuilding();
                    if (target != null)
                    {
                        state = State.ChaseBuilding;
                        GetComponent<AIDestinationSetter>().target = target.transform;
                        TargetFound = true;
                    }
                }
                break;
        }
            
        
        
        
        //Change the skin when walking
        if (aiPath.desiredVelocity.x >= 0.01f) transform.localScale = new Vector3(5.167f, 4.125f, 1f);
        else if (aiPath.desiredVelocity.x <= -0.01f) transform.localScale = new Vector3(-5.167f, 4.125f, 1f);
    }

    public GameObject FindClosestPlayer()
    {
        float DistanceToClosestPlayer = Mathf.Infinity;
        GameObject playerTarget = null;
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            float Distancetoplayer = (player.transform.position - transform.position).sqrMagnitude;
            if (Distancetoplayer < DistanceToClosestPlayer)
            {
                DistanceToClosestPlayer = Distancetoplayer;
                playerTarget = player;
            }
        }
        return playerTarget;
    }

    public bool CanChasePlayer(GameObject player)
    {
        if (Vector3.Distance(transform.position, player.transform.position) < range)
        {
            return true;
        }
        return false;
    }

    public GameObject FindClosestBuilding()
    {
        float DistanceToClosestBuilding = Mathf.Infinity;
        GameObject buildingTarget = null;
        foreach (GameObject building in GameObject.FindGameObjectsWithTag("Building"))
        {
            float Distancetobuilding = (building.transform.position - transform.position).sqrMagnitude;
            if (Distancetobuilding < DistanceToClosestBuilding)
            {
                DistanceToClosestBuilding = Distancetobuilding;
                buildingTarget = building;
            }
        }
        return buildingTarget;
    }
    
    public bool CanAttack()
    {
        if (target == null) return false;
        if (Vector3.Distance(transform.position, target.transform.position) <= attackrange)
        {
            if (target == null) return false;
            if (Time.time > nextattacktime)
            {
                float rateattack = 1f;
                nextattacktime = Time.time + rateattack;
                return true;
            }
            return false;
        }
        return false;
    }

    public void TargetLostPlayer(GameObject t)
    {
        GameObject[] monsters = GameObject.FindGameObjectsWithTag("monsteralpha");
        foreach (GameObject m in monsters)
        {
            BasicEnemy compo = m.GetComponent<BasicEnemy>();
            if (compo.target == t)
            { 
                compo.target = null;
                compo.state = State.Nothing;
                compo.TargetFound = false;
            }
        }
    }
    
    public void TargetLostBuildingB(GameObject t)
    {
        GameObject[] monsters = GameObject.FindGameObjectsWithTag("monsteralpha");
        foreach (GameObject m in monsters)
        {
            BasicEnemy compo = m.GetComponent<BasicEnemy>();
            if (compo.target == t)
            { 
                compo.target = null;
                compo.state = State.Nothing;
                compo.TargetFound = false;
            }
        }
        GameObject[] monstersb = GameObject.FindGameObjectsWithTag("monsterbeta");
        foreach (GameObject m in monstersb)
        {
            TankEnemy compo = m.GetComponent<TankEnemy>();
            if (compo.target == t)
            { 
                compo.target = null;
                compo.state = TankEnemy.State.Nothing;
                compo.TargetFound = false;
            }
        }
    }
}
