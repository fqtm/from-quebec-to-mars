﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using Pathfinding;
using Player.Scripts;
using UnityEngine;

public class TankEnemy : MonoBehaviour
{
    private float nextattacktime;
    public int damage;
    public float attackrange;
    public GameObject target; //Current target of the mob
    public bool TargetFound; //Bool if mob has a target
    public GameObject targetDestroyed;

    public AIPath aiPath; // Road of the mob
    
    public enum State
    {
        ChaseBuilding,
        Nothing
    }

    public State state;
    // Start is called before the first frame update
    void Start()
    {
        targetDestroyed = null;
        if (!TargetFound)
        {
            
            target = FindClosestBuilding();
            if (target != null)
            {
                state = State.ChaseBuilding;
                GetComponent<AIDestinationSetter>().target = target.transform;
                TargetFound = true;
            }
            else
            {
                state = State.Nothing;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Select the target

        switch (state)
        {
            case State.ChaseBuilding:
                if (CanAttack())
                {
                    if (target!=null) target.GetComponent<BuildingLife>().TakeDamage(damage);
                    if (target!=null)
                    {
                        if (target.GetComponent<BuildingLife>().GetDestroy())
                        {
                            AstarPath.active.UpdateGraphs (target.GetComponent<BoxCollider2D>().bounds);
                            targetDestroyed = target;
                            TargetLostBuildingT(target);
                            targetDestroyed.GetComponent<Building>().RemoveBuilding();
                        }
                    }
                    
                }
                break;
            case State.Nothing:
                target = FindClosestBuilding();
                if (target != null)
                {
                    state = State.ChaseBuilding;
                    GetComponent<AIDestinationSetter>().target = target.transform;
                    TargetFound = true;
                }
                break;
        }

        //Change the skin when walking
        if (aiPath.desiredVelocity.x >= 0.01f) transform.localScale = new Vector3(5.167f, 4.125f, 1f);
        else if (aiPath.desiredVelocity.x <= -0.01f) transform.localScale = new Vector3(-5.167f, 4.125f, 1f);
    }

    public GameObject FindClosestBuilding()
    {
        float DistanceToClosestBuilding = Mathf.Infinity;
        GameObject buildingTarget = null;
        foreach (GameObject building in GameObject.FindGameObjectsWithTag("Building"))
        {
            float Distancetobuilding = (building.transform.position - transform.position).sqrMagnitude;
            if (Distancetobuilding < DistanceToClosestBuilding)
            {
                DistanceToClosestBuilding = Distancetobuilding;
                buildingTarget = building;
            }
        }
        return buildingTarget;
    }
    
    public bool CanAttack()
    {
        if (target==null)return false;
        if (Vector3.Distance(transform.position, target.transform.position) <= attackrange)
        {
            if (target==null)return false;
            if (Time.time > nextattacktime)
            {
                float rateattack = 1f;
                nextattacktime = Time.time + rateattack;
                return true;
            }
            return false;
        }
        return false;
    }
    
    public void TargetLostBuildingT(GameObject t)
    {
        GameObject[] monsters = GameObject.FindGameObjectsWithTag("monsteralpha");
        foreach (GameObject m in monsters)
        {
            BasicEnemy compo = m.GetComponent<BasicEnemy>();
            if (compo.target == t)
            { 
                compo.target = null;
                compo.state = BasicEnemy.State.Nothing;
                compo.TargetFound = false;
            }
        }
        GameObject[] monstersb = GameObject.FindGameObjectsWithTag("monsterbeta");
        foreach (GameObject m in monstersb)
        {
            TankEnemy compo = m.GetComponent<TankEnemy>();
            if (compo.target == t)
            { 
                compo.target = null;
                compo.state = State.Nothing;
                compo.TargetFound = false;
            }
        }
    }
}
