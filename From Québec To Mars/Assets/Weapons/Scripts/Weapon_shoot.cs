﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Weapon_shoot : NetworkBehaviour
{

    public Transform firepoint;
    public GameObject bulletprefab;
    
    void Update()
    {
        
    }

    public void OnLClick()
    {
        if (isLocalPlayer && gameObject.GetComponent<PlayerNetwork>().WeaponOut)
            Shoot();
    }

    void Shoot()
    {
        CmdShoot(firepoint.position, firepoint.rotation);
    }

    [Command]
    public void CmdShoot(Vector3 position, Quaternion quaternion)
    {
        NetworkServer.Spawn(Instantiate(bulletprefab, position, quaternion));
    }
}
