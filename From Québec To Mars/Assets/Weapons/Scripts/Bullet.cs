﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Bullet : NetworkBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        EnemyLife enemy = hitInfo.gameObject.GetComponent<EnemyLife>();
        if (enemy != null)
        {
            enemy.TakeDamage(5);
        }
        Destroy(gameObject);
        
    }

    private void OnBecameInvisible()
    {
        enabled = false;
        Destroy(gameObject);
    }
}
