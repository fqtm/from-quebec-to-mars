﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using Mirror;
using UnityEngine.UI;

public class PlayerWeapon : NetworkBehaviour
{
    private Transform aimTransform;
    public SpriteRenderer muzzleflash;
    public Transform firepoint;
    
    [SyncVar] public bool shooting;
    public SpriteRenderer pistol;
    [SyncVar] public Vector2 aimDirection;

    private void Awake()
    {
        aimTransform = transform.Find("Aim");
    }

    private void Update()
    {
        if (isLocalPlayer)
        {
            Vector3 mousePosition = Utils.GetMouseWorldPosition();
            aimDirection = (mousePosition - transform.position).normalized;
            CmdUpdateAngle(aimDirection);
        }
        if (shooting)
        {
            shooting = false;
            muzzleflash.enabled = !muzzleflash.enabled;
        }
        else
        {
            muzzleflash.enabled = false;
        }
        HandleAiming();
    }

    private void HandleAiming()
    {
        float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        aimTransform.eulerAngles = new Vector3(0, 0, angle);
        firepoint.eulerAngles = new Vector3(0, 0, angle);
        pistol.flipY = angle > 90 || angle < -90;
    }

    [Command]
    private void CmdUpdateAngle(Vector2 aim)
    {
        aimDirection = aim;
    }


    private void OnLClick()
    {
        if (isLocalPlayer)
            shooting = true;
    }
}
