﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Player.Scripts;
using UnityEngine;

public class PlayerNetworkedSavingSystem : NetworkBehaviour
{
    [Command]
    public void CmdLoadPlayer()
    {
        NetworkIdentity player = ClientScene.localPlayer;
        try
        {
            player.GetComponent<PlayerNetwork>().Load();
            player.GetComponent<Inventory>().Load();//Loads host
            PlayerData playerData = PlayerSavingSystem.Load(2);
            InventoryData inventoryData = InventorySavingSystem.Load(2);
            RpcLoadPlayer(playerData.ToString(), inventoryData.ToString());//Loads client
        }
        catch
        {
            Debug.Log("Load Failed");
        }
    }

    [ClientRpc]
    private void RpcLoadPlayer(string playerDataStr, string inventoryDataStr)
    {
        NetworkIdentity player = ClientScene.localPlayer;
        Debug.Log("Load Client: " + !player.isServer);
        if (!player.isServer)
        {
            string[] playerDataArr = playerDataStr.Split(':');
            string[] inventoryDataArr = inventoryDataStr.Split('\n');
            player.GetComponent<PlayerNetwork>().LoadFromPlayerData(
                new PlayerData(new Vector2(
                        float.Parse(playerDataArr[0]), 
                        float.Parse(playerDataArr[1])),
                int.Parse(playerDataArr[2])));
            (uint, int)[] items = new (uint, int)[int.Parse(inventoryDataArr[0])];
            for (int i = 0; i < items.Length; i++)
            {
                string couple = inventoryDataArr[i + 1];
                string[] strings = couple.Split(',');
                items[i] = (uint.Parse(strings[0]), int.Parse(strings[1]));
            }

            player.GetComponent<Inventory>().LoadFromData(new InventoryData(items));
            player.GetComponent<OnConnectInitializer>().Initiate();
        }
    }

    [Command]
    public void CmdSaveClientPn(string data)
    {
        string[] playerDataArr = data.Split(':');
        PlayerSavingSystem.SaveFromData(new PlayerData(
            new Vector2(float.Parse(playerDataArr[0]), float.Parse(playerDataArr[1])), 
            int.Parse(playerDataArr[2])),2u);
    }

    [Command]
    public void CmdSaveClientInv(string data)
    {
        string[] inventoryDataArr = data.Split('\n');
        (uint, int)[] items = new (uint, int)[int.Parse(inventoryDataArr[0])];
        for (int i = 0; i < items.Length; i++)
        {
            string couple = inventoryDataArr[i + 1];
            string[] strings = couple.Split(',');
            items[i] = (uint.Parse(strings[0]), int.Parse(strings[1]));
        }
        InventorySavingSystem.SaveFromData(new InventoryData(items), 2u);
    }
}