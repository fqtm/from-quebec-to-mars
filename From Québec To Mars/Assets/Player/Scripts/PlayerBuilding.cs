﻿using System;
using System.Collections;
using System.Collections.Generic;
using MAP.Scripts;
using UnityEngine;
using Mirror;
using Player.Scripts;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PlayerBuilding : NetworkBehaviour
{
    [SerializeField] private GameObject selected_building;
    private PlayerNetwork _playerNetwork;
    [NonSerialized] public GameObject transparent;
    private bool selected;
    private bool hovering;
    private SpriteRenderer sr;
    private Map map;
    private GameObject[] buildings;
    [NonSerialized] public int[] Building_List;
    [NonSerialized] public bool destroy;
    public Button DestroyButton;
    [SerializeField] private GameObject MenuHost;
    [SerializeField] private GameObject MenuClient;
    private NetworkManager _networkManager;
    
    

    // Start is called before the first frame update
    void Start()
    {
        map = GameObject.FindWithTag("Map").GetComponent<Map>();
        selected = false;
        sr = GetComponent<SpriteRenderer>();
        hovering = false;
        buildings = GameObject.FindGameObjectWithTag("ID").GetComponent<ID>().buildings;
        if (Building_List == null)
            Building_List = new int[GameObject.FindGameObjectWithTag("ID").GetComponent<ID>().buildings.Length];
        _playerNetwork = GetComponent<PlayerNetwork>();
        Building_List[6] = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            map.gridMap.GetXY(transform.position, out _, out int y);
            sr.sortingOrder = map.gridMap.GetHeight() - y;
        }
        
    }

    public void OnEscape()
    {
        if (isLocalPlayer)
        {
            if (_playerNetwork.Tech_Tree_open)
            {
                Utils.Display(_playerNetwork.Tech_tree_UI,ref _playerNetwork.Tech_Tree_open);
            }

            if (_playerNetwork.isBuildingMenu)
            {
                Utils.Display(_playerNetwork.BuildingMenu,ref _playerNetwork.isBuildingMenu);
            }
            if (_playerNetwork.isInteracting)
                foreach (Building building in FindObjectsOfType<Building>())
                    building.OnEscape();
            else if (selected)
            {
                ResetSelected();
                StopHover();
                if (destroy)
                    SetDestroy();
            }
            else if (isServer)
                Utils.Display(MenuHost.GetComponent<CanvasGroup>(),ref _playerNetwork.isEscapeMenu);
            else
                Utils.Display(MenuClient.GetComponent<CanvasGroup>(),ref _playerNetwork.isEscapeMenu);
            
            
        }
        
    }

    public GameObject GetHoverBuilding()
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = Input.mousePosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, raycastResults);
        for (int i = 0; i < raycastResults.Count; i++)
        {
            if (raycastResults[i].gameObject.CompareTag("Building"))
            {
                return raycastResults[i].gameObject;
            }
        }

        return null;
    }

    public void OnLClick()
    {
        if (isLocalPlayer)
        {
            if (selected && !hovering && !Utils.IsOverUI())
            {
                Build();
            }
        }
    }

    public void SetSelected(int i)
    {
        StopHover();
        if (!selected && Building_List[i] > 0)
        {
          selected = true;
          selected_building = Utils.GetBuilding((uint)i);
          Hover();  
        }
    }

    public void SetDestroy()
    {
        destroy = !destroy;
        DestroyButton.GetComponent<Image>().color = destroy ? Color.red : Color.white;
    }

    private void ResetSelected()
    {
        selected = false;
    }

    public void Build()
    {
        map.gridMap.GetXY(Utils.GetMouseWorldPosition(), out int x, out int y);
        Building_Specifications bs = selected_building.GetComponent<Building_Specifications>();
        if (map.gridMap.IsEmpty(x, y, x + bs.GetWidth(), y + bs.GetHeight()) &&
            (!selected_building.GetComponent<Building_Specifications>().GetTransparent().GetComponent<Transparent>().isDrilling 
             || map.gridMap.IsOnOre(x, y, x + bs.GetWidth(), y + bs.GetHeight())) && Building_List[bs.GetId()] > 0)
        {
            CmdBuild(map.gridMap.GetWorldPosition(x, y), bs.GetId(), x, y);
        }
    }

    public void UnlockBuilding(int i)
    {
        Building_List[i] += 3;
        GetComponent<BuildingMenu>().UpdateCount(i);
    }

    public void UpdateBuildingCount(int i, Text text)
    {
        if (Building_List == null)
            Building_List = new int[GameObject.FindGameObjectWithTag("ID").GetComponent<ID>().buildings.Length];
        text.text = Building_List[i].ToString();
    }

    [Command]
    public void CmdBuild(Vector3 WorldPos, uint id, int x, int y)
    {
        GameObject selected_building = Utils.GetBuilding(id);
        Building_Specifications bs = selected_building.GetComponent<Building_Specifications>();
        GameObject instantiated = Instantiate(selected_building, WorldPos, Quaternion.identity);
        instantiated.GetComponent<SpriteRenderer>().sortingOrder = map.gridMap.GetHeight() - y;
        NetworkServer.Spawn(instantiated);
        map.gridMap.SetSaving_Building(x, y, instantiated);
        map.gridMap.Fill(x, y, x + bs.GetWidth(), y + bs.GetHeight(), true, instantiated);
        RpcFillMap(x, y, instantiated,id);
        instantiated.GetComponent<BuildingLife>().CreateHealthBar();
        AstarPath.active.UpdateGraphs (instantiated.GetComponent<BoxCollider2D>().bounds); // Refresh the graph for AI pathfinding
    }

    [ClientRpc]
    public void RpcFillMap(int x, int y, GameObject instantiated,uint id) //Fills the map on each client
    {
        Building_Specifications bs = instantiated.GetComponent<Building_Specifications>();
        map.gridMap.SetSaving_Building(x, y, instantiated);
        map.gridMap.Fill(x, y, x + bs.GetWidth(), y + bs.GetHeight(), true, instantiated);
        Building_List[id]--;
        GetComponent<BuildingMenu>().UpdateCount((int) id);
        instantiated.GetComponent<Drilling_Machine>()?.Initiate();
    }

    
    public void Hover()
    {
        GameObject transparent = selected_building.GetComponent<Building_Specifications>().GetTransparent();
        map.gridMap.GetXY(Utils.GetMouseWorldPosition(), out int x, out int y);
        GameObject instantiate = Instantiate(transparent, map.gridMap.GetWorldPosition(x, y), Quaternion.identity);
        instantiate.GetComponent<SpriteRenderer>().sortingOrder = map.gridMap.GetHeight() - y;
        this.transparent = instantiate;
    }

    public void StopHover()
    {
        Destroy(transparent);
        transparent = null;
        selected_building = null;
        selected = false;
    }


    public void OnRotate()
    {
        if (selected_building == null)
            return;
        Destroy(transparent);
        transparent = null;
        Hover();
    }
    
}