using System;
using System.Collections.Generic;
using Mirror;
using Player.Scripts;
using UnityEngine;

public class PlayersDatas : NetworkBehaviour
{
    public Queue<PlayerData> PlayerDatas;

    private void Start()
    {
        PlayerDatas = new Queue<PlayerData>();
    }

    public void Save()
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            player.GetComponent<PlayerNetwork>().Save();
        }
    }

    public void Load()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        uint id = (uint)players.Length;
        foreach (GameObject player in players)
        {
            PlayerNetwork pn = player.GetComponent<PlayerNetwork>();
            if (pn.id == id)
                pn.Load();
        }
    }
}