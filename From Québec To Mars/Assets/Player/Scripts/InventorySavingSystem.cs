using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
public class InventorySavingSystem
{
    public static string path = Application.persistentDataPath + "/inventories/inventory";
    public static void Save(Inventory inventory, uint id)
    {
        SaveFromData(new InventoryData(inventory), id);
    }

    public static void SaveFromData(InventoryData data, uint id)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/inventories"))
            Directory.CreateDirectory(Application.persistentDataPath + "/inventories");
        BinaryFormatter formatter = new BinaryFormatter();
        string path = InventorySavingSystem.path + id + ".txt";
        FileStream stream = new FileStream(path, FileMode.Create);
        
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static InventoryData Load(uint id)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/inventories"))
            return null;
        string path = InventorySavingSystem.path + id + ".txt";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            InventoryData data = formatter.Deserialize(stream) as InventoryData;
            stream.Close();
            return data;
        }
            
        Debug.LogError("File not found: " + path);
        return null;
    }
}
