[System.Serializable]
public class InventoryData
{
    public (uint, int)[] items;

    public InventoryData(Inventory inventory)
    {
        int l = inventory.slots.Length;
        items = new (uint,int)[l];
        for (int i = 0; i < l; i++)
        {
            if (inventory.slots[i].transform.childCount == 1)
                items[i] = (0, -1);
            else
                items[i] = (Utils.GetID(inventory.slots[i].transform.GetChild(1).gameObject),
                    inventory.slots[i].GetComponent<Slot>().GetNb());
        }
    }

    public InventoryData((uint, int)[] items)
    {
        this.items = items;
    }

    public override string ToString()
    {
        string str = items.Length + "\n";
        foreach ((uint id, int n) in items)
        {
            str += id + "," + n + "\n";
        }

        return str;
    }
}
