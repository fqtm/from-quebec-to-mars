﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Player.Scripts;
using UnityEngine.UI;

public class Inventory : NetworkBehaviour
{
    [NonSerialized] public bool[] isFull;
    [NonSerialized] public GameObject[] slots;
    public GameObject panel;
    public CanvasGroup menu;
    private bool IsMenuOpened;


    void Start()
    {
        if (!isLocalPlayer)
        {
            foreach (Canvas can in GetComponentsInChildren<Canvas>())
            {
                can.enabled = false;
            }
        }
        else
        {
            var childCount = panel.transform.childCount;
            slots = new GameObject[childCount];
            isFull = new bool[childCount];
            for (int i = 0; i < childCount; i++)
            {
                GameObject Slot = panel.transform.GetChild(i).gameObject;
                slots[i] = Slot;
                Slot.GetComponent<Slot>().i = i;
            }
        }
    }

    public void Save()
    {
        if (isServer)
            InventorySavingSystem.Save(this, GetComponent<PlayerNetwork>().id);
        else
            GetComponent<PlayerNetworkedSavingSystem>().CmdSaveClientInv(new InventoryData(this).ToString());
    }

    public void Load()
    {
        LoadFromData(InventorySavingSystem.Load(GetComponent<PlayerNetwork>().id));
    }

    public void LoadFromData(InventoryData data)
    {
        if (data == null)
            return;
        foreach (GameObject slot in slots)
            slot.GetComponent<Slot>().Clear();
        
        for (int i = 0; i < data.items.Length; i++)
        {
            isFull[i] = data.items[i].Item2 != -1;
            if (isFull[i])
            {
                Instantiate(Utils.GetResourceButton(data.items[i].Item1), slots[i].transform, false);
                slots[i].GetComponent<Slot>().UpdateSlot(data.items[i].Item2);
            }
        }
    }

    public void OnInventory()
    {
        Utils.Display(menu, ref IsMenuOpened);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (isLocalPlayer)
        {
            if (other.CompareTag("Pickup"))
            {
                GameObject pickup = other.gameObject;
                Pickup Pick = pickup.GetComponent<Pickup>();
                int i = -1;
                for (int j = 0; j < slots.Length; j++)
                {
                    Slot slot = slots[j].GetComponent<Slot>();
                    if (isFull[j] &&
                        slot.GetCO().GetComponent<Spawn>().item.GetComponent<Pickup>().GetId() == Pick.GetId() &&
                        slot.GetNb() < Utils.MaxStack)
                    {
                        slot.IncreaseNb(1);
                        CmdPickup(pickup);
                        i = -1;
                        break;
                    }

                    if (i == -1 && !isFull[j])
                    {
                        i = j;
                    }
                }

                if (i != -1)
                {
                    GameObject button = Instantiate(pickup.GetComponent<Pickup>().itemButton, slots[i].transform,
                        false);
                    Slot slot = slots[i].GetComponent<Slot>();
                    slot.stack = button.GetComponentInChildren<Text>();
                    slot.SetCO(button);
                    slot.IncreaseNb(1);
                    slot.removeButton.interactable = true;
                    isFull[i] = true;
                    CmdPickup(pickup);
                }
            }
        }
    }

    [Command]
    public void CmdPickup(GameObject ressource)
    {
        NetworkServer.Destroy(ressource);
    }


    public void DropItem(int i)
    {
        if (isLocalPlayer)
        {
            GameObject button = null;
            bool found = false;
            Transform slot = slots[i].transform;
            int j = 0;
            while (j < slot.childCount)
            {
                Transform child = slot.GetChild(j);
                if (child.CompareTag("Button"))
                {
                    found = true;
                    button = child.gameObject;
                    break;
                }

                j++;
            }

            if (found)
            {
                slot.gameObject.GetComponent<Slot>().IncreaseNb(-1);
                CmdDropItem(button.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId());
                if (slot.GetComponent<Slot>().GetNb() <= 0)
                {
                    slot.GetComponent<Slot>().removeButton.interactable = false;
                    isFull[i] = false;
                    Destroy(button);
                }
            }
            else
            {
                Debug.Log("GameObject is null");
            }
        }
    }

    [Command]
    public void CmdDropItem(uint id)
    {
        GameObject ressource = Instantiate(Utils.GetResource(id),
            transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        NetworkServer.Spawn(ressource);
    }

    public void ClearSlots()
    {
        foreach (GameObject slot in slots)
        {
            slot.GetComponent<Slot>().Clear();
        }
    }
}