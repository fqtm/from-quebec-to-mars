using System;
using System.Collections;
using MAP.Scripts;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;

public class OnConnectInitializer : NetworkBehaviour
{
    private bool isMapInitialized;
    public void Start()
    {
        Initiate();
    }

    public void Initiate()
    {
        if (!isClientOnly || !isLocalPlayer)
            return;
        CmdInitiateMap();
        CmdInitiateTechTree();
        StartCoroutine(InitiateDrilling());
    }

    public IEnumerator InitiateDrilling()
    {
        while (!isMapInitialized)
        {
            yield return null;
        }
        foreach (Drilling_Machine drillingMachine in FindObjectsOfType<Drilling_Machine>())
            drillingMachine.Initiate();
    }

    [Command]
    public void CmdInitiateMap()
    {
        NetworkConnection conn = connectionToClient;
        Map map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
        TargetInitiateMap(conn, new MapData(map).ToString());
        foreach (Building building in FindObjectsOfType<Building>())
        {
            if (!(building is StartingBase))
            {
                //NetworkServer.Spawn(building.gameObject);
                var position = building.transform.position;
                TargetInitiateBuilding(conn, new BuildingData(building, (int) position.x, (int) position.y).ToString(),
                    building.GetComponent<NetworkIdentity>().netId);
            } 
        }
    }

    [TargetRpc]
    public void TargetInitiateMap(NetworkConnection conn, string data)
    {
        Map map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
        map.LoadMapFromString(data);
        isMapInitialized = true;
    }

    [TargetRpc]
    public void TargetInitiateBuilding(NetworkConnection conn, string data, uint netId)
    {
        GameObject.FindGameObjectWithTag("Map").GetComponent<Map>().LoadBuildingFromString(data, netId);
    }

    [Command]
    public void CmdInitiateTechTree()
    {
        TechTree_Manager techtree = GetComponent<TechTree_Manager>();
        Technologies technologies = FindObjectOfType<PlayerTechTree>().getPlayersTech();
        foreach (Technologies.TechType tech in technologies.unlockedTechTypeList)
        {
            techtree.UnlockTech((int)tech);
        }
    }
}