using Mirror;
using Unity.Mathematics;
using UnityEngine;

public class BuildingNetwork : NetworkBehaviour
{
        [Command]
        public void CmdUpdateSlot(GameObject building, int slot, uint id, int n)
        { 
            RpcUpdateSlot(building, slot, id, n);
        }

        [Command]
        public void CmdClearSlot(GameObject building, int slot)
        {
            RpcClearSlot(building, slot);
        }

        [Command]
        public void CmdIncreaseSlotNb(GameObject building, int slot, int n)
        {
            RpcIncreaseSlotNb(building, slot, n);
        }

        [ClientRpc]
        public void RpcUpdateSlot(GameObject building, int slot, uint id, int n)
        {
            BuildingSlot Slot = building.GetComponent<Building>().GetSlot(slot);
            if (Slot == null)
            {
                Debug.Log("Slot is null");
                return;
            }

            if (Slot.transform.childCount == 0)
                Instantiate(Utils.GetResourceButton(id), Slot.transform, false);
            Slot.UpdateSlotAfterCall(n);
        }

        [ClientRpc]
        public void RpcClearSlot(GameObject building, int slot)
        {
            building.GetComponent<Building>().GetSlot(slot).ClearAfterCall();
        }

        [ClientRpc]
        public void RpcIncreaseSlotNb(GameObject building, int slot, int n)
        {
            building.GetComponent<Building>().GetSlot(slot)?.IncreaseNbAfterCall(n);
        }

        [Command]
        public void CmdSetAssemblyRecipe(GameObject assemblyMachine, uint craftingRecipe)
        {
            RpcSetAssemblyRecipe(assemblyMachine, craftingRecipe);
        }

        [ClientRpc]
        public void RpcSetAssemblyRecipe(GameObject assemblyMachine, uint craftingRecipe)
        {
            assemblyMachine.GetComponentInChildren<CraftingRecipeUI>().SetRecipeAfterCall(Utils.GetRecipe(craftingRecipe));
        }

        [Command]
        public void CmdSpawnDrop(uint drop,Vector3 pos)
        {
            NetworkServer.Spawn(Instantiate(Utils.GetResource(drop),pos,quaternion.identity));
        }
        
        [Command]
        public void CmdLoot(uint drop,Vector3 pos)
        {
            NetworkServer.Spawn(Instantiate(Utils.GetResource(drop),pos,quaternion.identity));
        }
}
