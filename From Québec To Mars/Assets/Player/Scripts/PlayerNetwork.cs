﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Net.Mail;
using UnityEngine;
using Mirror;
using Player.Scripts;
using Unity.Mathematics;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Composites;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.VFX;


public class PlayerNetwork : NetworkBehaviour
{
    [Header("Player parameters")] 
    public float speed;
    public int maxHealthPlayer = 100;

    [SyncVar(hook = "SetHealth")] [NonSerialized]
    public int currentHealthPlayer;


    [Header("Linked Game Objects")] [SerializeField]
    private new Camera camera;
    public CanvasGroup Tech_tree_UI;
    public CanvasGroup BuildingMenu;
    public Slider slider;
    private float side;
    private Animator animator;
    private Vector2 movement;
    public bool Tech_Tree_open;
    [NonSerialized] public bool isInteracting;
    [NonSerialized] public bool isEscapeMenu;
    [NonSerialized] public bool isBuildingMenu;
    public uint id;
    public GameObject StartingBase;
    public bool WeaponOut;
    [SerializeField] private GameObject aim;
    public bool UnlockWeapon;

    // Start is called before the first frame update
    void Start()
    {
        if (isLocalPlayer)
        {
            Tech_Tree_open = false;
            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.enabled = true;
            animator = GetComponent<Animator>();
            animator.enabled = true;
            camera = Instantiate(camera.gameObject, transform.position, Quaternion.identity).GetComponent<Camera>();
            camera.gameObject.GetComponent<CameraFollow>().target = transform;
            WeaponOut = false;
            UnlockWeapon = false;
            currentHealthPlayer = maxHealthPlayer; // Set life of player to 100
            SetMaxHealth(); // Set the slide to 100 of HealthBar
            aim.SetActive(false);
            if (isServer)
            {
                StartingBase = Instantiate(StartingBase, Vector3.zero, Quaternion.identity);
                NetworkServer.Spawn(StartingBase);
            }
            else
            {
                StartingBase = FindObjectOfType<StartingBase>().gameObject;
            }
        }

        id = isServer ? 1u : 2u;
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            //Animation
            if (movement.x != 0 && !isInteracting)
            {
                side = movement.x;
            }

            if (isInteracting || isEscapeMenu)
            {
                movement = Vector2.zero;
            }

            animator.SetFloat("Horizontal", side);
            animator.SetFloat("Speed", movement.sqrMagnitude);
        }
    }

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            movement = movement.normalized;
            //Movement

            transform.Translate(Vector2.up * (speed * movement.y * Time.deltaTime));
            transform.Translate(Vector2.right * (speed * movement.x * Time.deltaTime));
        }
    }

    public void Save()
    {
        if (isServer)
            PlayerSavingSystem.Save(this, id);
        else
        {
            PlayerData data = new PlayerData(this);
            GetComponent<PlayerNetworkedSavingSystem>().CmdSaveClientPn(data.ToString());
        }
    }

    public void SaveGame()
    {
        FindObjectOfType<SaveSystem>().SaveGame();
    }

    public void Load()
    {
        LoadFromPlayerData(PlayerSavingSystem.Load(id));
    }

    public void LoadFromPlayerData(PlayerData data)
    {
        if (data == null)
            return;
        transform.position = new Vector3(data.position[0], data.position[1], 0);
        currentHealthPlayer = data.life;
    }


    //Input commands
    public void OnVertical(InputValue vertical)
    {
        if (isLocalPlayer)
        {
            movement.y = vertical.Get<float>();
        }
    }

    public void OnHorizontal(InputValue horizontal)
    {
        if (isLocalPlayer)
        {
            movement.x = horizontal.Get<float>();
        }
    }

    private void OnTech_Tree()
    {
        if (isLocalPlayer)
        {
            if (!isInteracting && !isBuildingMenu)
            {
                Utils.Display(Tech_tree_UI, ref Tech_Tree_open);
            }
            
        }
    }
    
    public void OnBuilding()
    {
        if (isLocalPlayer)
        {
            if (!isInteracting)
            {
                Utils.Display(BuildingMenu, ref isBuildingMenu);
            }
            
        }
    }
    


    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    /*
    Partie sur la vie du joueur 
    Téo 
    Partie sur la vie du joueur
    */


    public void SetMaxHealth()
    {
        slider.value = slider.maxValue;
    }

    public void SetHealth(int oldvalue, int newvalue)
    {
        slider.value = newvalue;
    }

    public void TakeDamage(int amount)
    {
        if (!isServer) return;
        currentHealthPlayer -= amount;
        if (currentHealthPlayer <= 0)
        {

            currentHealthPlayer = 0;
        }

        if(!isServer) return;
            currentHealthPlayer -= amount;
            if (currentHealthPlayer<=0)
            {
                currentHealthPlayer = 0;
            }
    }

        public void Respawn()
        {
            GetComponent<Inventory>().ClearSlots();
            GetComponent<Transform>().position = new Vector3(0, 0, 0);
        }

        public StartingBase GetStartingBase()
        {
            return StartingBase.GetComponent<StartingBase>();
        }

        private void OnWeaponHide()
        {
            Debug.Log("Armmmmeeee");
            if (!WeaponOut && UnlockWeapon)
            {
                aim.SetActive(true);
                WeaponOut = true;
            }
            else
            {
                aim.SetActive(false);
                WeaponOut = false;
            }
            
        }
        
}
