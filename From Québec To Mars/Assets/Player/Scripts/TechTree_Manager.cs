﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Player.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class TechTree_Manager : NetworkBehaviour
{
    
    [NonSerialized] public Technologies _playersTech;

    private BuildingMenu Menu;
    public Text text;
    
    public void Start()
    {
        if (isLocalPlayer)
        {
            _playersTech = GameObject.FindGameObjectWithTag("Tech_tree").GetComponent<PlayerTechTree>().getPlayersTech();
            _playersTech.OnTechUnlocked += PlayerTech_OnTechUnlocked;
            Menu = ClientScene.localPlayer.GetComponent<BuildingMenu>();
            UpdateCountTechPoint();
        }
    }

    public void UnlockTech(int n)
    {
        CmdUnlock(n);
    }

    public void UnlockPoutine()
    {
        Assembly_machine[] assembly = FindObjectsOfType<Assembly_machine>();
        foreach (var ass in assembly)
        {
            ass.DisplayPoutine();
        }
        
    }
    public void Save()
    {
        TechSavingSystem.Save(_playersTech);
    }

    public void Load()
    {
        TechData data = TechSavingSystem.Load();
        _playersTech = new Technologies();
        _playersTech.TechPoint = Int32.MaxValue;
        foreach (int tech in data.UnlockedTech)
        {
            UnlockTech(tech);
        }
        _playersTech.TechPoint = data.TechPoint;
    }
    
    
    private void PlayerTech_OnTechUnlocked(object sender, Technologies.OnTechUnlockedEventArgrs e)
    {
        PlayerNetwork player = GetComponent<PlayerNetwork>();
        PlayerBuilding playerBuilding = GetComponent<PlayerBuilding>();
        switch (e.TechType)
        {
            case Technologies.TechType.Basic_Build:
                playerBuilding.UnlockBuilding(0);
                playerBuilding.UnlockBuilding(1);
                playerBuilding.UnlockBuilding(2);
                Menu.ShowButton(0);
                Menu.ShowButton(1);
                Menu.ShowButton(2);
                break;
            case Technologies.TechType.Chest_Conveyor:
                playerBuilding.UnlockBuilding(3);
                playerBuilding.UnlockBuilding(4);
                Menu.ShowButton(3);
                Menu.ShowButton(4);
                break;
            case Technologies.TechType.Weapon:
                gameObject.GetComponent<PlayerNetwork>().UnlockWeapon = true;
                break;
            case Technologies.TechType.Tesla_Wall:
                playerBuilding.UnlockBuilding(4);
                playerBuilding.UnlockBuilding(5);
                Menu.ShowButton(5);
                Menu.ShowButton(6);
                break;
            case Technologies.TechType.Basic_Build2:
                break;
            case Technologies.TechType.Speed:
                player.SetSpeed(10f);
                break;
            case Technologies.TechType.LifeUpgrade1:
                player.maxHealthPlayer = 150;
                player.SetHealth(100,150);
                break;
            case Technologies.TechType.LifeUpgrade2:
                player.maxHealthPlayer = 200;
                player.SetHealth(150,200);
                break;
            case Technologies.TechType.Weapon2:
                break;
            case Technologies.TechType.Tesla_Wall2:
                break;
            case Technologies.TechType.Poutine:
                break;
        }
    }
    
    public void UpdateCountTechPoint()
    {
        text.text = _playersTech.TechPoint.ToString();
    }
    
    [Command]
    public void CmdUnlock(int n)
    {
        RpcUnlock(n);
    }

    [Command]
    public void CmdIncreaseTechPoint()
    {
        RpcIncreaseTechPoint();
    }
    [ClientRpc]
    public void RpcUnlock(int n)
    {
        Technologies _playersTech = ClientScene.localPlayer.GetComponent<TechTree_Manager>()._playersTech;
        if (_playersTech.TryUnlockTech((Technologies.TechType) n))
        {
            foreach (UI_TechTree button in ClientScene.localPlayer.gameObject.GetComponentsInChildren<UI_TechTree>())
            {
                if (button.tech == (Technologies.TechType) n)
                {
                    button.Enable();
                    UpdateCountTechPoint();
                    Debug.Log("Unlock" + (Technologies.TechType) n);
                    if ((Technologies.TechType) n == Technologies.TechType.Poutine)
                    {
                        UnlockPoutine();
                    }
                }
            }
        }
    }

    [ClientRpc]
    public void RpcIncreaseTechPoint()
    {
        StartingBase startingBase = ClientScene.localPlayer.GetComponent<PlayerNetwork>().GetStartingBase();
        startingBase.getInventory().RemoveItem(startingBase.ItemRequire, startingBase.AmoutRequire);
        startingBase.AmoutRequire += 3;
        startingBase.UpdateCountTechPoint();
        _playersTech.AddTechpoint(1);
        UpdateCountTechPoint();
    }
}
