using System.Globalization;
using Mirror;
using Player.Scripts;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public float[] position;
    public int life;

    public PlayerData(PlayerNetwork playerNetwork)
    {
        var position1 = playerNetwork.transform.position;
        position = new[] {position1.x, position1.y};
        life = playerNetwork.currentHealthPlayer;
    }

    public PlayerData(Vector2 pos, int health)
    {
        position = new[] {pos.x, pos.y};
        life = health;
    }
    public override string ToString()
    {
        return position[0].ToString(CultureInfo.CurrentCulture) + ":" +
               position[1].ToString(CultureInfo.CurrentCulture) + ":" +
               life;
    }
}


