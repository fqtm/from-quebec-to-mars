using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Player.Scripts
{
    public static class PlayerSavingSystem
    {
        public static string path = Application.persistentDataPath + "/players/player";
        public static void Save(PlayerNetwork playerNetwork, uint id)
        {
            SaveFromData(new PlayerData(playerNetwork), id);
        }

        public static void SaveFromData(PlayerData data, uint id)
        {
            if (!Directory.Exists(PlayerSavingSystem.path))
                Directory.CreateDirectory(Application.persistentDataPath + "/players");
            BinaryFormatter formatter = new BinaryFormatter();
            string path = PlayerSavingSystem.path + id + ".txt";
            FileStream stream = new FileStream(path, FileMode.Create);
            
            formatter.Serialize(stream, data);
            stream.Close();
        }

        public static PlayerData Load(uint id)
        {
            if (!Directory.Exists(Application.persistentDataPath + "/players"))
                return null;
            string path = PlayerSavingSystem.path + id + ".txt";
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                PlayerData data = formatter.Deserialize(stream) as PlayerData;
                stream.Close();
                return data;
            }
            
            Debug.LogError("File not found: " + path);
            return null;
        }
    }
}