﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class BuildingMenu : MonoBehaviour
{
    [SerializeField] 
    public CanvasGroup[] list_Button_building;

    private PlayerBuilding PlayerBuilding;


    private void Start()
    {
        PlayerBuilding = ClientScene.localPlayer.GetComponent<PlayerBuilding>();
        for (int i = 0; i < list_Button_building.Length; i++)
        {
            UpdateCount(i);
        }
    }

    public void ShowButton(int i)
    {
        bool test = false;
        Utils.Display(list_Button_building[i],ref test);
    }

    public void UpdateCount(int i)
    {
        PlayerBuilding.UpdateBuildingCount(i,list_Button_building[i].GetComponentsInChildren<Text>()[0]);
    }
    
}
