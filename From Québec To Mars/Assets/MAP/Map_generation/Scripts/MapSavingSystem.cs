using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MAP.Scripts;
using Pathfinding;
using Player.Scripts;
using UnityEngine;

public static class MapSavingSystem
{
    public static void Save(Map map)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/map.txt";
        FileStream stream = new FileStream(path, FileMode.Create);

        MapData data = new MapData(map);
            
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static MapData Load()
    {
        string path = Application.persistentDataPath + "/map.txt";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            MapData data = formatter.Deserialize(stream) as MapData;
            stream.Close();
            return data;
        }
            
        Debug.LogError("File not found: " + path);
        return null;
            
    }
}
