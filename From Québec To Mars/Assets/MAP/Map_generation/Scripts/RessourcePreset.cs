﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace MAP.Scripts
{
     [CreateAssetMenu(fileName = "Ressource Preset", menuName = "New Ressource Preset")]
     public class RessourcePreset : ScriptableObject
     {
          public Sprite[] tiles;
          public float minHeight;
          public float minMoisture;
          public float minHeat;
          public Tile.TileNature Nature;
          public Tile.OreNature Ore;
     
          public Sprite GetTileSprite()
          {
               return tiles[Random.Range(0,tiles.Length)];
          }
     
          public bool MatchCondition(float height, float moisture, float heat)
          {
               return height >= minHeight && moisture >= minMoisture && heat >= minHeat;
          }
          
     }
}


