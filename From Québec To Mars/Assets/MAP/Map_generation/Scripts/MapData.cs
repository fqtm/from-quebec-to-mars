using System;
using System.Collections.Generic;
using MAP.Scripts;
using UnityEngine;

[Serializable]
public class MapData
{
    public float offsetX;
    public float offsetY;
    public int width;
    public int height;
    public BuildingData[] buildings;
    public (uint,bool)[,] buildings_bool;

    public MapData(Map map)
    {
        offsetX = map.offsetX;
        offsetY = map.offsetY;
        width = map.width;
        height = map.height;
        buildings_bool = new (uint, bool)[height, width];
        map.gridMap.GetBuilding_Bool();
        List<BuildingData> buildings = new List<BuildingData>();
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject building = map.gridMap.GetSaving_Buildings()[y, x];
                if (building != null)
                {
                    buildings_bool[y, x] = (Utils.GetID(map.gridMap.GetBuilding_Bool()[y, x].Item1),
                        map.gridMap.GetBuilding_Bool()[y, x].Item2);
                    buildings.Add(new BuildingData(building.GetComponent<Building>(), x, y));
                }
            }
        }

        this.buildings = buildings.ToArray();
    }

    public override string ToString()
    {
        return offsetX + "\n" + offsetY + "\n" + width + "\n" + height;
    }
}