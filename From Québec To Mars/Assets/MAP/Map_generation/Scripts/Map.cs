﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Player.Scripts;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;


namespace MAP.Scripts
{
    public class Map : NetworkBehaviour
    {
        public GridMap gridMap;
        public RessourcePreset[] resources;
        public GameObject tilePrefab;

        [Header("Dimensions")]
        [SyncVar] public int width = 50;
        [SyncVar] public int height = 50;
        [SyncVar] public float scale = 1.0f;
        [SyncVar] public float offsetX;
        [SyncVar] public float offsetY;
        
        [Header("Height Map")]
        public Wave[] heightWaves;
        public float[,] heightMap;
    
        
        [Header("Moisture Map")]
        public Wave[] moistureWaves;
        private float[,] moistureMap;
    
       
        [Header("Heat Map")]
        public Wave[] heatWaves;
        private float[,] heatMap;

        void Start()
        {
            if (isServer) //Generates a new map settings
            {
                gridMap = new GridMap(width, height, scale, new Vector3(Mathf.Round(width * -0.5f),
                    Mathf.Round(height * -0.5f))); // Change: Math.Round add
                offsetX = Random.Range(0f,9999);
                offsetY = Random.Range(0f,9999);
                GenerateMap();
            }
        }
        

        public void Save()
        {
            MapSavingSystem.Save(this);
        }

        public void Load()
        {
            if (!isServer) return;
            MapData data = MapSavingSystem.Load();

            width = data.width;
            height = data.height;
            offsetX = data.offsetX;
            offsetY = data.offsetY;
            gridMap = new GridMap(width, height, scale, new Vector3(Mathf.Round(width * -0.5f),
                Mathf.Round(height * -0.5f)));
            (GameObject, bool)[,] arr = new (GameObject, bool)[height, width];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    arr[y, x] = (Utils.GetBuilding(data.buildings_bool[y, x].Item1), data.buildings_bool[y, x].Item2);
                }
            }
            gridMap.SetBuilding_Bool(arr);
            foreach (BuildingData buildingData in data.buildings)
            {
                GameObject instantiated = Instantiate(Utils.GetBuilding(buildingData.ID),
                    gridMap.GetWorldPosition(buildingData.X, buildingData.Y), Quaternion.identity);
                switch (buildingData.ID)
                {
                    case 0:
                        Furnace furnace = instantiated.GetComponent<Furnace>();
                        gridMap.SetSaving_Building(buildingData.X, buildingData.Y, instantiated);
                        if (buildingData.inputs[0].Item2 != 0)
                        {
                            furnace.AddResource(buildingData.inputs[0].Item2, 
                                Utils.GetResourceButton(buildingData.inputs[0].Item1));
                        }

                        if (buildingData.inputs[1].Item2 != 0)
                        {
                            furnace.AddFuel(buildingData.inputs[1].Item2);
                        }

                        if (buildingData.outputs[0].Item2 != 0)
                        {
                            Instantiate(Utils.GetResourceButton(buildingData.outputs[0].Item1),
                                furnace.OutputSlot.transform, false);
                            furnace.OutputSlot.UpdateSlot(buildingData.outputs[0].Item2);
                        }

                        furnace.meltingTime = buildingData.progress;
                        furnace.fuelRemaining = buildingData.fuel;
                        break;
                    case 1:
                        Drilling_Machine drillingMachine = instantiated.GetComponent<Drilling_Machine>();
                        drillingMachine.currentTime = buildingData.progress;
                        for (int i = 0; i < 3; i++)
                        {
                            if (buildingData.outputs[i].Item2 != 0)
                            {
                                Instantiate(Utils.GetResourceButton(buildingData.outputs[i].Item1),
                                    drillingMachine.outputs[i].transform, false);
                                drillingMachine.outputs[i].UpdateSlot(buildingData.outputs[i].Item2);
                            }
                        }
                        break;
                    case 2:
                        //Assembly Machine
                        break;
                }
                NetworkServer.Spawn(instantiated);
            }
            GenerateMap();
        }

        public void LoadMapFromString(string data)
        {
            string[] arr = data.Split('\n');
            offsetX = float.Parse(arr[0]);
            offsetY = float.Parse(arr[1]);
            width = int.Parse(arr[2]);
            height = int.Parse(arr[3]);
            gridMap = new GridMap(width, height, scale, new Vector3(Mathf.Round(width * -0.5f),
                Mathf.Round(height * -0.5f)));
            GenerateMap();
        }

        public void LoadBuildingFromString(string data, uint netId)
        {
            foreach (NetworkIdentity identity in FindObjectsOfType<NetworkIdentity>())
            {
                if (identity.netId == netId)
                {
                    Building building = identity.GetComponent<Building>();
                    building.Load(data);
                    Building_Specifications bs = identity.GetComponent<Building_Specifications>();
                    int x = building.X;
                    int y = building.Y;
                    gridMap.Fill(x, y, x + bs.GetWidth(), y + bs.GetHeight(), true, identity.gameObject);
                    break;
                }
            }
        }
        

        public void GenerateMap()
        {
            foreach (GameObject tile in GameObject.FindGameObjectsWithTag("Tile"))
            {
                Destroy(tile);
            }

            // height map
            heightMap = NoiseGenerator.Generate(width, height, scale, heightWaves, offsetX, offsetY);
    
            // moisture map
            moistureMap = NoiseGenerator.Generate(width, height, scale, moistureWaves, offsetX, offsetY);
    
            // heat map
            heatMap = NoiseGenerator.Generate(width, height, scale, heatWaves, offsetX, offsetY);
            
            for(int x = 0; x < width; ++x)
            {
                for(int y = 0; y < height; ++y)
                {
                    GameObject tile = Instantiate(tilePrefab, gridMap.GetWorldPosition(x,y), Quaternion.identity);
                    SpriteRenderer sr = tile.GetComponent<SpriteRenderer>();
                    sr.sortingOrder = -1;
                    RessourcePreset rp = getRessource(heightMap[x, y], moistureMap[x, y], heatMap[x, y]);
                    sr.sprite = rp.GetTileSprite();
                    tile.GetComponent<Tile>().tileNature = rp.Nature;
                    tile.GetComponent<Tile>().oreNature = rp.Ore;
                    gridMap.ores[y, x] = rp.Ore;
                }
            }
        }
    
        public class RessourceTempData
        {
            public RessourcePreset _ressource;
            public RessourceTempData (RessourcePreset preset)
            {
                _ressource = preset;
            }
            
            public float GetDiffValue (float height, float moisture, float heat)
            {
                return (height - _ressource.minHeight) + (moisture - _ressource.minMoisture) + (heat - _ressource.minHeat);
            }
        }
        
        public RessourcePreset getRessource(float height, float moisture, float heat)
        {
            RessourcePreset RessourceToReturn = null;
            List<RessourceTempData> ressourceTemp = new List<RessourceTempData>();
            foreach (RessourcePreset ressource in resources)
            {
                if (ressource.MatchCondition(height, moisture, heat))
                {
                    ressourceTemp.Add(new RessourceTempData(ressource));
                }
            }

            float curVal = 0.0f;

            foreach (RessourceTempData ressource in ressourceTemp)
            {
                if (RessourceToReturn == null)
                {
                    RessourceToReturn = ressource._ressource;
                    curVal = ressource.GetDiffValue(height, moisture, heat);
                }
                else
                {
                    if (ressource.GetDiffValue(height, moisture, heat) < curVal)
                    {
                        RessourceToReturn = ressource._ressource;
                        curVal = ressource.GetDiffValue(height, moisture, heat);
                    }
                }
            }

            if (RessourceToReturn == null)
                RessourceToReturn = resources[0];

            return RessourceToReturn;
        }
        
    }
}