﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public TileNature tileNature;
    public OreNature oreNature;
    public enum TileNature
    {
        Ground,
        Ore
    }

    public enum OreNature //Follows the ID order
    {
        Iron,
        Copper,
        Coal,
        None
    }
}
