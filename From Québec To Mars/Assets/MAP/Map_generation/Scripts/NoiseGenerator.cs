﻿using UnityEngine;

namespace MAP.Scripts
{
    public class NoiseGenerator : MonoBehaviour
    {
        public static float[,] Generate (int width, int height, float scale, Wave[] waves, float offsetX, float offsetY)
        {
            // crée la map
            float[,] noiseMap = new float[width, height];
            
            for(int x = 0; x < width; ++x)
            {
                for(int y = 0; y < height; ++y)
                {
                    // calcule une position une position 
                    float samplePosX = (float)x * scale + offsetX;
                    float samplePosY = (float)y * scale + offsetY;
     
                    float normalization = 0.0f;
                    
                    foreach(Wave wave in waves)
                    {
                        noiseMap[x, y] += wave.amplitude * Mathf.PerlinNoise(samplePosX * wave.frequency + wave.seed, samplePosY * wave.frequency + wave.seed);
                        normalization += wave.amplitude;
                    }
                    
                    noiseMap[x, y] /= normalization;
                }
            }
            
            return noiseMap;
            
        }
        
    }
    [System.Serializable]
    public class Wave
    {
        public float seed;
        public float frequency;
        public float amplitude;
    }
}


