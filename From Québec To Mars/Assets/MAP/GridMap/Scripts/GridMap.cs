﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using TMPro.Examples;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Assertions.Must;
using Vector3 = UnityEngine.Vector3;

public struct GridMap
{
    private int[,] Grid;
    private GameObject[,] Saving_buildings;
    [NonSerialized] public Tile.OreNature[,] ores;
    private int width;
    private int height;
    private Vector3 originPosition;
    private float cellSize;
    private TextMesh[,] debugTextArray;
    private (GameObject, bool)[,] Buildings; //Test new system build
    
    public GridMap(int width, int height, float cellSize, Vector3 originPosition)
    {
        this.height = height;
        this.width = width;
        this.cellSize = cellSize;
        this.originPosition = originPosition;
        Grid = new int[height, width];
        Saving_buildings = new GameObject[height, width];
        ores = new Tile.OreNature[height, width];
        debugTextArray = new TextMesh[height, width];
        Buildings = new (GameObject, bool)[height,width];


        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                //debugTextArray[x, y] = Utils.CreateWorldText(Grid[x, y].ToString(), null, GetWorldPosition(x, y) + new Vector3(cellSize,cellSize) * .5f, 8, Color.white,
                //TextAnchor.MiddleCenter);
                //Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.white, 100f);
                //Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.white, 100f);
            }
        }

        //Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        //Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);
    }

    public void RemoveBuilding(GameObject gameObject)
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (Buildings[y, x].Item1 == gameObject)
                    Buildings[y, x] = (null, false);
                if (Saving_buildings[y, x] == gameObject)
                    Saving_buildings[y, x] = null;
            }
        }
    }

    public float GetCellSize()
    {
        return cellSize;
    }

    public int GetHeight()
    {
        return height;
    }

    public void SetBuilding_Bool((GameObject, bool)[,] arr)
    {
        Buildings = arr;
    }

    public (GameObject,bool)[,] GetBuilding_Bool()
    {
        return Buildings;
    }

    public void SetSaving_Building(int x, int y, GameObject gameObject)
    {
        Saving_buildings[y, x] = gameObject;
    }

    public void SetSaving_Building(GameObject[,] arr)
    {
        Saving_buildings = arr;
    }

    public GameObject[,] GetSaving_Buildings()
    {
        return Saving_buildings;
    }

    public void Fill(int x1, int y1, int x2, int y2, bool value, GameObject go)
    {
        if (x2 < x1)
            (x1, x2) = (x2, x1);
        if (y2 < y1)
            (y1, y2) = (y2, y1);
        if (x1 < 0 || y1 < 0)
            return;
        for (int i = x1 ; i < width && i < x2; i++)
        {
            for (int j = y1 ; j < height && j < y2; j++)
            {
                Buildings[j, i] = (go,value);
            }
        }
    }

    public bool IsEmpty(int x1, int y1, int x2, int y2)//Test new system build
    {
        if (x2 < x1)
            (x1, x2) = (x2, x1);
        if (y2 < y1)
            (y1, y2) = (y2, y1);
        if (x1 < 0 || y1 < 0)
            return false;
        bool empty = true;
        for (int i = x1; i < width && i < x2 && empty; i++)
        {
            for (int j = y1; j < height && j < y2 && empty; j++)
            {
                empty = !Buildings[j, i].Item2;
            }
        }
    
        return empty;
    }
    
    public bool IsOnOre(int x1, int y1, int x2, int y2)
    {
        if (x2 < x1)
            (x1, x2) = (x2, x1);
        if (y2 < y1)
            (y1, y2) = (y2, y1);
        if (x1 < 0 || y1 < 0)
            return false;
        bool isNotOre = true;
        for (int i = x1; i < width && i < x2 && isNotOre; i++)
        {
            for (int j = y1; j < height && j < y2 && isNotOre; j++)
            {
                isNotOre = ores[j, i] == Tile.OreNature.None;
            }
        }

        return !isNotOre;
    }


    public Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(x, y) * cellSize + originPosition;
    }

    public void GetXY(Vector3 worldPosition, out int x, out int y)
    {
        x = Mathf.FloorToInt((worldPosition - originPosition).x / cellSize);
        y = Mathf.FloorToInt((worldPosition - originPosition).y / cellSize);
        if (x < 0)
            x = 0;
        if (x >= width)
            x = width - 1;
        if (y < 0)
            y = 0;
        if (y >= height)
            y = height - 1;
    }

    public void SetValue(int x, int y, int value)
    {
        if (x >= 0 && y >= 0 && x < width && y < height)
        {
            Grid[x, y] = value;
            debugTextArray[y, x].text = Grid[y, x].ToString();
        }
    }

    public void SetValue(Vector3 worldPosition, int value)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);
        SetValue(x, y, value);
    }

    public int GetValue(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < width && y < height)
            return Grid[y, x];
        return -1;
    }

    public int GetValue(Vector3 worldPosition)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);
        return GetValue(x, y);
    }
}