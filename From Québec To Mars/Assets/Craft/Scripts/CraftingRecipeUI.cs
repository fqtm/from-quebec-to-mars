﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class CraftingRecipeUI : MonoBehaviour
{

    public Assembly_machine assemblyMachine;
    private BuildingNetwork BuildingNetwork;

    private void Start()
    {
        BuildingNetwork = ClientScene.localPlayer.GetComponent<BuildingNetwork>();
    }


    public void SetRecipe(CraftingRecipe craftingRecipe)
    {
        BuildingNetwork.CmdSetAssemblyRecipe(transform.root.gameObject, craftingRecipe.id);
    }

    public void SetRecipeAfterCall(CraftingRecipe craftingRecipe)
    {
        assemblyMachine.SelectRecipe(craftingRecipe);
    }

   
    
   


}