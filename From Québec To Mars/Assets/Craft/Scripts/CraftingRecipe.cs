﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;



 [Serializable]   
public struct ItemAmout
{
    
    public GameObject Item;
    [Range(1,64)]
    public int Amount;
}

[CreateAssetMenu(fileName = "Crafting Recipe", menuName = "New Crafting Recipe")]
public class CraftingRecipe : ScriptableObject
{
    public uint id;
    public List<ItemAmout> Materials;
    public ItemAmout Results;
    public float TimeCrafting;

    public bool CanCraft(Building_inventory inventory,BuildingSlot result)
    {
        if (result.go != null && Utils.GetID(result.go) != Utils.GetID(Results.Item))
            return false;
        if (!result.CanAdd(Results.Amount))
            return false;
        foreach (var item in Materials)
        {
            if (inventory.ItemCount(item.Item) < item.Amount)
            {
                return false;
            }
        }

        return true;
    }

    public void Craft(Building_inventory inventory, BuildingSlot result)
    {
        if (CanCraft(inventory,result))
        {
            foreach (ItemAmout item in Materials)
            {
                inventory.RemoveItem(item.Item, item.Amount);
            }
            if (ClientScene.localPlayer.isServer)
                result.AddItem(Results.Item, Results.Amount);
        }
    }
    
    public void Craft(Building_inventory inventory)
    {
        foreach (var item in Materials)
        {
            if (inventory.ItemCount(item.Item) < item.Amount)
            {
                
                return ;
            }
            
        }
        foreach (ItemAmout item in Materials)
        {
            inventory.RemoveItem(item.Item, item.Amount);
        }
        ClientScene.localPlayer.GetComponent<PlayerBuilding>().UnlockBuilding((int)Utils.GetID(Results.Item));
    }
}
    
    
    
    
    

