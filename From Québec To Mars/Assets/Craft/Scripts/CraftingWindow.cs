﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;

public class CraftingWindow : MonoBehaviour
{
    [SerializeField] private CraftingRecipeUI RecipeUI;
    [SerializeField] private RectTransform recipeUIParent;
    [SerializeField] private List<CraftingRecipeUI> _craftingRecipeUIs;

    public List<CraftingRecipe> CraftingRecipes;

   
}
