﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecipeImage : MonoBehaviour
{
    public Assembly_machine assemblyMachine;
    public Image[] images;


    public void Start()
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].sprite = null;
            images[i].GetComponent<CanvasGroup>().alpha = 0;
        }
    }

    public void UpdateImage()
    {
        for (int j = 0; j < images.Length ; j++)
        {
            if (j < assemblyMachine.getCraftingRecipe().Materials.Count)
            {
                images[j].sprite = assemblyMachine.getCraftingRecipe().Materials[j].Item.GetComponent<Image>().sprite;
                images[j].GetComponent<CanvasGroup>().alpha = 0.6f;
                images[j].GetComponentInChildren<Text>().text =
                    assemblyMachine.getCraftingRecipe().Materials[j].Amount.ToString();
            }
            else
            {
                images[j].sprite = null;
                images[j].GetComponent<CanvasGroup>().alpha = 0;
                images[j].GetComponentInChildren<Text>().text = "";
            }

            
            
        }
    }

    public void HideImage(int i)
    {

        images[i].GetComponent<CanvasGroup>().alpha = 0f;
        
    }
}
