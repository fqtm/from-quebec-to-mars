﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject LoadingScreen;
    [SerializeField] Slider _slider;
    [SerializeField] TMP_Text _text;

    public void LoadLevel(int Sceneindex)
    {
        StartCoroutine(LoadAsynchronously(Sceneindex));
    }

    IEnumerator LoadAsynchronously(int SceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneIndex);
        LoadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            _slider.value = progress;
            _text.text = "L O A D I N G   "+ progress * 100f+"%" ;

            yield return null;
        }
    }
}
