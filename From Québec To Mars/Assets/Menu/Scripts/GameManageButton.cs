﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManageButton : MonoBehaviour
{
    [SerializeField] NetworkManager manager;
    [SerializeField] Canvas canvasmenu;
    [SerializeField] GameObject menustart;

    public void StartHost ()
    {
        canvasmenu.enabled = false;
        Destroy(menustart);
        manager.StartHost();
        
    }
    
    public void StartHostLoad ()
    {
        canvasmenu.enabled = false;
        Destroy(menustart);
        manager.StartHost();
        // TO DO LOAD ALL THING
    }

    public void JoinGame()
    {
        canvasmenu.enabled = false;
        Destroy(menustart);
        manager.networkAddress = "localhost";
        manager.StartClient();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
