﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public void ChangeSceneHost(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
        NetworkManager.singleton.StopHost();
    }
    
    public void ChangeSceneClient(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
        NetworkManager.singleton.StopClient();
    }
    
    public void QuitGame()
    {
        NetworkServer.Shutdown();
        Application.Quit();
    }
}
