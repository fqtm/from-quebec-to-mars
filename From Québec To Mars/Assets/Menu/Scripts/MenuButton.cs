﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour
{
	[SerializeField] MenuButtonController menuButtonController;
	[SerializeField] Animator animator;
	[SerializeField] AnimatorFunctions animatorFunctions;
	[SerializeField] int thisIndex;
	[SerializeField] GameObject pixel;
	[SerializeField]  GameObject menu;
	[SerializeField]  GameObject newmenu;
	[SerializeField]  GameObject CVD;
	[SerializeField]  int scene;
	private AudioManager audioManager;
	private Button button;
	private Image image;
	private Image pixelimage;
	

    // Update is called once per frame
    private void Start()
    {
	    image = GetComponent<Image>();
	    pixelimage = pixel.GetComponent<Image>();
	    button = GetComponent<Button>();
	    audioManager = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
	    if(menuButtonController.index == thisIndex)
	    {
		    animator.SetBool ("selected", true);
		    image.enabled = true;
		    pixelimage.enabled = true;
		    if(Input.GetAxis ("Submit") == 1){
			    animator.SetBool ("pressed", true);
		    }else if (animator.GetBool ("pressed")){
			    button.onClick.Invoke();
		    }
	    }else{
		    animator.SetBool ("selected", false);
		    image.enabled = false;
		    pixelimage.enabled = false;
	    }
    }

    public void SelectedButton()
    {
	    menuButtonController.index = thisIndex;
    }

    public void changemenu()
    {
	    if (!animator.GetBool("pressed"))
	    {
		    animator.SetBool ("pressed", true);
	    }
	    animator.SetBool ("pressed",  false);
	    audioManager.Play("ButtonPressed");
	    animatorFunctions.disableOnce = true;
	    menu.SetActive(false);
	    newmenu.SetActive(true);
    }
    
    public void StartNewGameButton()
    {
	    if (!animator.GetBool("pressed"))
	    {
		    animator.SetBool ("pressed", true);
	    }
	    animator.SetBool ("pressed",  false);
	    animatorFunctions.disableOnce = true;
	    menu.SetActive(false);
    }
    
    public void QuitButton()
    {
	    if (!animator.GetBool("pressed"))
	    {
		    animator.SetBool ("pressed", true);
	    }
	    animator.SetBool ("pressed",  false);
	    animatorFunctions.disableOnce = true;
	    Application.Quit();
    }

    public void ColorChange()
    {
	    if (!animator.GetBool("pressed"))
	    {
		    animator.SetBool ("pressed", true);
	    }
	    animator.SetBool ("pressed",  false);
	    animatorFunctions.disableOnce = true;
	    CVD.GetComponent<CVDFilter>().SwitchColorprof(thisIndex-1);
    }

    public void ChangeScene()
    {
	    SceneManager.LoadScene(scene);
    }
}
