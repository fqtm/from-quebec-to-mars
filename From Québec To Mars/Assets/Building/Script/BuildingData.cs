
using System;
using System.Linq;

[System.Serializable]
public class BuildingData
{
    public uint ID;
    public float progress;
    public float fuel;
    public (uint, int)[] inputs;
    public (uint, int)[] outputs;
    public int X;
    public int Y;

    public BuildingData(Building building, int X, int Y)
    {
        if (building is Furnace furnace)
            Furnace(furnace);
        else if (building is Drilling_Machine drillingMachine)
            DrillingMachine(drillingMachine);
        else if (building is Assembly_machine assemblyMachine)
            AssemblyMachine(assemblyMachine);
        else if (building is StartingBase)
            return;
        this.X = X;
        this.Y = Y;
    }
    public void Furnace(Furnace furnace)
    {
        ID = 0;
        progress = furnace.meltingTime;
        fuel = furnace.fuelRemaining;
        inputs = new[] {
            (Utils.GetID(furnace.OreSlot.go), furnace.OreSlot.nb),
            (2u,furnace.FuelSlot.nb)
        };
        outputs = new[]
        {
            (Utils.GetID(furnace.OutputSlot.go), furnace.OutputSlot.nb)
        };
    }

    public void DrillingMachine(Drilling_Machine drillingMachine)
    {
        ID = 1;
        progress = drillingMachine.currentTime;
        fuel = -1;
        inputs = null;
        outputs = new []
        {
            (Utils.GetID(drillingMachine.outputs[0].go),drillingMachine.outputs[0].nb),
            (Utils.GetID(drillingMachine.outputs[1].go),drillingMachine.outputs[1].nb),
            (Utils.GetID(drillingMachine.outputs[2].go),drillingMachine.outputs[2].nb)
        };
    }

    public void AssemblyMachine(Assembly_machine assemblyMachine)
    {
        ID = 2;
        progress = assemblyMachine.Time;
        fuel = -1;
        inputs = new[]
        {
            (Utils.GetID(assemblyMachine.getInventory().GetInventory()[0].go),
                assemblyMachine.getInventory().GetInventory()[0].nb),
            (Utils.GetID(assemblyMachine.getInventory().GetInventory()[1].go),
                assemblyMachine.getInventory().GetInventory()[1].nb),
            (Utils.GetID(assemblyMachine.getInventory().GetInventory()[2].go),
                assemblyMachine.getInventory().GetInventory()[2].nb)
        };
        outputs = new[]
        {
            (Utils.GetID(assemblyMachine.result.go), assemblyMachine.result.nb)
        };
    }

    public override string ToString()
    {
        string str = ID + "\n";
        str += progress + "\n";
        str += fuel + "\n";
        if (inputs != null)
            foreach ((uint id, int n) in inputs)
                str += id + "," + n + "\n";
            

        str += "-" + "\n";
        if (outputs != null)
            foreach ((uint id, int n) in outputs)
            {
                str += id + "," + n + "\n";
            }

        str += X + "\n" + Y;
        return str;
    }
}
