﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class Assembly_machine : Building , ICatchable
{

    [SerializeField] 
    public BuildingSlot result;
    [SerializeField]
    private Slider Slider;
    [SerializeField] 
    private RecipeImage Images;

    public CanvasGroup poutine;


    private Building_inventory _inventory;
    private CraftingRecipe RecipeSelection;
    [NonSerialized][SyncVar] public float Time;
    private float CraftSpeed;
    private bool IScrafting;
    private BuildingMenu Menu;

    private new void Awake()
    {
        ((Building) this).Awake();
    }

    public new void Start()
    {
        ((Building)this).Start();
        _inventory = GetComponent<Building_inventory>();
        Menu = ClientScene.localPlayer.GetComponent<BuildingMenu>();
        RecipeSelection = null;
        PlayerBuilding playerBuilding = ClientScene.localPlayer.GetComponent<PlayerBuilding>();
        Slider.value = -1;
        if (ClientScene.localPlayer.GetComponent<TechTree_Manager>()._playersTech.IsTechUnlocked(Technologies.TechType.Poutine))
        {
            DisplayPoutine();
        }
    }

    public Building_inventory getInventory()
    {
        return _inventory;
    }

    public bool GetIscrafting()
    {
        return IScrafting;
    }

    public CraftingRecipe getCraftingRecipe()
    {
        return RecipeSelection;
    }

   

    public void SelectRecipe(CraftingRecipe craftingRecipe)
    {
        RecipeSelection = craftingRecipe;
        Time = craftingRecipe.TimeCrafting;
        IScrafting = RecipeSelection.CanCraft(_inventory, result);
        Images.UpdateImage();
    }

    public void Assembly_Craft()
    {
        if (RecipeSelection.Results.Item.CompareTag("Building"))
        {
            Debug.Log("Test");
            RecipeSelection.Craft(_inventory);
        }
        else
        {
            RecipeSelection.Craft(_inventory,result);
        }
    }

    public void DisplayPoutine()
    {
        bool temp = false;
        Utils.Display(poutine,ref temp);
    }

    public GameObject PushObject()
    {
        GameObject temp = result.go;
        result.RemoveItem(1);
        return Utils.GetResource(Utils.GetID(temp));
    }



    public void Update()
    {
        if (RecipeSelection != null)
        {
            if (RecipeSelection.CanCraft(_inventory, result))
            {
                if (Time > 0)
                {
                    Time -= UnityEngine.Time.deltaTime;
                    Slider.value = -Time / RecipeSelection.TimeCrafting;
                }
                else
                {
                    Assembly_Craft();
                    IScrafting = RecipeSelection.CanCraft(_inventory, result);
                    Time = RecipeSelection.TimeCrafting;
                    Slider.value = -1;
                    Debug.Log("craft");
                }
            }
            else
            {
                Slider.value = -1;
            }
        }
    }

    public bool UpdateInputs(GameObject go, GameObject parent, int nb)
    {
        parent.GetComponent<BuildingSlot>()?.AddItem(Utils.GetResourceButton(Utils.GetID(go)), nb);
        return true;
    }

    public void UpdateSlotInput()
    {
        
    }

    public override BuildingSlot GetSlot(int i)
    {
        if (i < 0 || i > 3)
            return null;
        return i == 3 ? result : _inventory.GetInventory()[i].GetComponent<BuildingSlot>();
    }

    public void Load(string[] data)
    {
        Time = int.Parse(data[1]);
        for (int i = 0; i < 3; i++)
        {
            BuildingSlot slot = _inventory.GetInventory()[i];
            string[] couple = data[i + 3].Split(',');
            (uint id, int n) = (uint.Parse(couple[0]), int.Parse(couple[1]));
            if (n > 0)
            {
                Instantiate(Utils.GetResourceButton(id), slot.transform, false);
                slot.UpdateSlotAfterCall(n);
            }
        }
        
        string[] couple2 = data[7].Split(',');
        (uint id2, int n2) = (uint.Parse(couple2[0]), int.Parse(couple2[1]));
        if (n2 > 0)
        {
            Instantiate(Utils.GetResourceButton(id2), result.transform, false);
            result.UpdateSlotAfterCall(n2);
        }
        
    }
}
