﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Mirror;
using Pathfinding;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Furnace : Building , ICatchable , IPullable
{
    public Slider meltSlider;

    private Ore ore;
    public BuildingSlot OreSlot;
    [NonSerialized][SyncVar] public float meltingTime;
    public Slider fuelSlider;
    private Fuel fuel;
    public BuildingSlot FuelSlot;
    [NonSerialized][SyncVar] public float fuelRemaining;
    public BuildingSlot OutputSlot;
    private Animator animator;
    private ICatchable _catchableImplementation;
    private NetworkIdentity playerNetworkIdentity;

    private new void Awake()
    {
        ((Building) this).Awake();
    }
    
    // Start is called before the first frame update
    private new void Start()
    {
        ((Building)this).Start();
        animator = GetComponent<Animator>();
        playerNetworkIdentity = ClientScene.localPlayer;
        if (fuel is null)
        {
            fuelSlider.value = 0;
        }
        else
        {
            fuelSlider.value = fuelRemaining / fuel.capacity;
        }

        if (ore is null)
        {
            meltSlider.value = 10;
        }
        else
        {
            meltSlider.value = -(meltingTime / ore.melting_time);
        }
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("Active", !(ore is null) && !(fuel is null));
        if (OreSlot.nb > 0)
        {
            if (fuelRemaining > 0)
            {
                fuelRemaining -= Time.deltaTime;
                fuelSlider.value = fuelRemaining / fuel.capacity;
                meltingTime -= Time.deltaTime;
                meltSlider.value = -(meltingTime / ore.melting_time);
                if (meltingTime <= 0)
                {
                    IncreaseOreCount(-1);
                    if (OutputSlot.nb < 1)
                    {
                        OutputSlot.go = Instantiate(ore.melted, OutputSlot.transform, false);
                        OutputSlot.UpdateSlot(1);
                    }
                    else
                        IncreaseOutputCount(1);
                    if (OreSlot.nb > 0)
                    {
                        meltingTime = ore.melting_time;
                    }
                }
            }
            else if (FuelSlot.nb > 0)
            {
                IncreaseFuelCount(-1);
                fuelRemaining = fuel.capacity;
                fuelSlider.value = fuel.capacity;
                meltingTime -= Time.deltaTime;
                meltSlider.value = -(meltingTime / ore.melting_time);
                if (FuelSlot.nb < 1)
                {
                    Destroy(fuel.gameObject);
                }

                if (meltingTime <= 0)
                {
                    IncreaseOreCount(-1);
                    if (OutputSlot.nb < 1)
                    {
                        OutputSlot.go = Instantiate(ore.melted, OutputSlot.transform, false);
                        OutputSlot.UpdateSlot(1);
                    }
                    else
                        IncreaseOutputCount(1);
                }
            }
            else
            {
                if (meltingTime != 0 && meltingTime / ore.melting_time < 0.01)
                {
                    meltingTime = 0;
                    IncreaseOreCount(-1);
                    if (OutputSlot.nb < 1)
                    {
                        OutputSlot.go = Instantiate(ore.melted, OutputSlot.transform, false);
                        OutputSlot.UpdateSlot(1);
                    }
                    else
                        IncreaseOutputCount(1);
                }

                fuelSlider.value = 0;
                fuel = null;
            }
        }
        else
        {
            meltSlider.value = -1;
            if (!(ore is null) )
            {
                if (OreSlot.transform.childCount > 0)
                    Destroy(ore.gameObject);
                ore = null;
            }
        }
    }

    public bool AddResource(int nb, GameObject go)
    {
        uint ID = Utils.GetID(go);
        if (ore != null && Utils.GetID(go) != Utils.GetID(ore.gameObject))
            return false;
        if (OreSlot.nb < 1)
        {
            go = Instantiate(Utils.GetResourceButton(ID), OreSlot.transform, false);
            ore = go.GetComponent<Ore>();
            OreSlot.UpdateSlot(nb);
            meltingTime = ore.melting_time;
        }
        else
        {
            OreSlot.IncreaseNb(nb);
        }
        return true;
    }
    

    public void AddFuel(int nb)
    {
        if (FuelSlot.nb == 0)
        {
            GameObject go = Instantiate(Utils.GetResourceButton(2), FuelSlot.transform, false);
            fuel = go.GetComponent<Fuel>();
            FuelSlot.UpdateSlot(nb);
        }
        else
        {
            FuelSlot.IncreaseNb(nb);
        }
    }

    public void IncreaseOreCount(int i)
    {
        if (!playerNetworkIdentity.isServer) return;
        OreSlot.IncreaseNb(i);
    }

    public void IncreaseFuelCount(int i)
    {
        if (!playerNetworkIdentity.isServer) return;
        FuelSlot.IncreaseNb(i);
    }

    public void IncreaseOutputCount(int i)
    {
        if (!playerNetworkIdentity.isServer) return;
        OutputSlot.IncreaseNb(i);
    }

    public bool UpdateInputs(GameObject drop, GameObject parent, int nb)
    {
        if (parent.name == "Fuel_Slot" && drop.GetComponent<Fuel>() != null)
        {
            AddFuel(nb);
            return true;
        }
        if (parent.name == "Ressource_Slot" && drop.GetComponent<Ore>() != null)
        {
            return AddResource(nb, drop);
        }
        
        return false;
    }

    public void UpdateSlotInput()
    {
        if (ore == null)
        {
            ore = OreSlot.GetComponentInChildren<Ore>();
            if (ore != null)
                meltingTime = ore.melting_time;
        }

        if (fuel == null)
        {
            fuel = FuelSlot.GetComponentInChildren<Fuel>();
            if (fuel != null)
                fuelRemaining = fuel.capacity;
        }
    }

    public void Load(string[] data)
    {
        meltingTime = float.Parse(data[1]);
        fuelRemaining = float.Parse(data[2]);
        string[] couple = data[3].Split(',');
        (uint ID, int n) = (uint.Parse(couple[0]), int.Parse(couple[1]));
        if (n > 0)
        {
            AddResource(n, Utils.GetResourceButton(ID));
        }
        
        couple = data[4].Split(',');
        (ID, n) = (uint.Parse(couple[0]), int.Parse(couple[1]));
        if (n > 0)
        {
            AddFuel(n);
        }
        
        couple = data[6].Split(',');
        (ID, n) = (uint.Parse(couple[0]), int.Parse(couple[1]));
        if (n > 0)
        {
            OutputSlot.go = Instantiate(Utils.GetResourceButton(ID), OutputSlot.transform, false);
            IncreaseOutputCount(n);
        }
    }

    public override BuildingSlot GetSlot(int i)
    {
        switch (i)
        {
            case 0: //Resource
                return OreSlot;
            case 1: //Coal
                return FuelSlot;
            case 2: //Out
                return OutputSlot;
            default:
                return null;
        }
    }

    public GameObject PushObject()
    {
        if (OutputSlot.go == null)
        {
            return null;
        }
        GameObject rtn = Utils.GetResource(Utils.GetID(OutputSlot.go));
        if (OutputSlot.nb == 1)
        {
            OutputSlot.Clear();
            OutputSlot.ClearAfterCall();
            Destroy(OutputSlot.go);
        }
        else if (ClientScene.localPlayer.isServer)
        {
            OutputSlot.IncreaseNb(-1);
        }
        return rtn;
    }
}