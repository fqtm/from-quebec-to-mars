﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPullable
{
    GameObject PushObject();
    
    
}
