﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : Building, ICatchable
{
    public Building_inventory _inventory;
    
    private new void Awake()
    {
        ((Building) this).Awake();
    }

    private new void Start()
    {
        ((Building) this).Start();
    }
    public bool UpdateInputs(GameObject go, GameObject parent, int nb)
    {
        parent.GetComponent<BuildingSlot>()?.AddItem(Utils.GetResourceButton(Utils.GetID(go)), nb);
        return true;
    }

    public void UpdateSlotInput()
    {
    }
    
    public override BuildingSlot GetSlot(int i)
    {
        if (i < 0 || i > 17)
            return null;
        return  _inventory.GetInventory()[i].GetComponent<BuildingSlot>();
    }
}
