﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Building_Specifications : NetworkBehaviour
{
    [SerializeField] private int width;
    [SerializeField] private int height;
    [SerializeField] private uint id; 
    [SerializeField] private GameObject transparent;
    
    
    public int GetWidth()
    {
        return width;
    }

    public int GetHeight()
    {
        return height;
    }

    public uint GetId()
    {
        return id;
    }

    public GameObject GetTransparent()
    {
        return transparent;
    }
    
}
