using System;
using MAP.Scripts;
using Mirror;
using Mirror.Cloud.ListServerService;
using Player.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;


public class Building : NetworkBehaviour
{
    private SpriteRenderer sr;
    private PlayerBuilding player;
    private PlayerNetwork PlayerNetwork;
    private Map map;
    public int X;
    public int Y;
    private BuildingNetwork BuildingNetwork;
    
    
    public void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
        var position = transform.position;
        (X, Y) = ((int)position.x, (int)position.y);
    }

    public void Start()
    {
        if (player != null) return;
        player = ClientScene.localPlayer.GetComponent<PlayerBuilding>();
        PlayerNetwork = player.GetComponent<PlayerNetwork>();
        BuildingNetwork = player.GetComponent<BuildingNetwork>();
        var position = transform.position;
        (X, Y) = ((int)position.x, (int)position.y);
    }

    public void OnEscape()
    {
        Interactable interactable = GetComponentInChildren<Interactable>();
        if (interactable != null && interactable.IsInteracting && player.transparent == null)
        {
            interactable.Interact();
            PlayerNetwork.isInteracting = !PlayerNetwork.isInteracting;
            StopHover();
        }
    }

    private void OnDestroy()
    {
        OnEscape();
        foreach (BasicEnemy enemy in FindObjectsOfType<BasicEnemy>())
        {
            if (enemy.target == gameObject)
                enemy.target = null;
        }
        foreach (TankEnemy enemy in FindObjectsOfType<TankEnemy>())
        {
            if (enemy.target == gameObject)
                enemy.target = null;
        }
    }

    public void Hover()
    {
        if (player == null)
        {
            player = ClientScene.localPlayer.GetComponent<PlayerBuilding>();
            PlayerNetwork = player.GetComponent<PlayerNetwork>();
        }
        if (player.destroy)
            sr.color = Color.red;
        else if (player.transparent == null && !PlayerNetwork.isInteracting)
            sr.color = Color.yellow;
    }
    public void StopHover()
    {
        sr.color = Color.white;
    }

    public void InteractBuilding()
    {
        if (GetComponentInChildren<Canvas>() == null)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
            }
        }
        if (player.destroy)
        {
            RemoveBuilding();
        }
        else
        {
            Interactable interactable = GetComponentInChildren<Interactable>();
            if (interactable != null && player.transparent == null && !PlayerNetwork.isInteracting)
            {
                if (PlayerNetwork.isBuildingMenu)
                    PlayerNetwork.OnBuilding();
                interactable.Interact();
                PlayerNetwork.isInteracting = !PlayerNetwork.isInteracting;
                sr.color = Color.white;
            }
        }
    }

    public void Load(string data)
    {
        string[] arr = data.Split('\n');
        switch (arr[0])
        {
            case "0":
                GetComponent<Furnace>().Load(arr);
                break;
            case "1":
                GetComponent<Drilling_Machine>().Load(arr);
                break;
            case "2":
                GetComponent<Assembly_machine>().Load(arr);
                break;
        }
    }

    public virtual BuildingSlot GetSlot(int i)
    {
        Debug.Log("niet");
        return null;
    }

    public void RemoveBuilding()
    {
        map.gridMap.RemoveBuilding(gameObject);
        NetworkServer.Destroy(gameObject);
    }
    
}
