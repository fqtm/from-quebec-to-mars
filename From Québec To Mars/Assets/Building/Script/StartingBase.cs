﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class StartingBase : Building, ICatchable
{

    
    public GameObject ItemRequire;
    public int AmoutRequire;
    public Text text;

    private TechTree_Manager Technologies;
    [SerializeField] private Building_inventory Inventory;
    
    
    public new void Awake()
    {
        ((Building)this).Awake();

    }
    
    
    
    public void UpdateCountTechPoint()
    {
        text.text = AmoutRequire.ToString();
    }

    public Building_inventory getInventory()
    {
        return Inventory;
    }

    public new void Start()
    {
        ((Building)this).Start();
        Technologies = ClientScene.localPlayer.GetComponent<TechTree_Manager>();
        Inventory = GetComponent<Building_inventory>();
        AmoutRequire = 1;
        UpdateCountTechPoint();
    }
    


    public bool CanUnlockedTechPoint()
    {
        if (Inventory.ItemCount(ItemRequire) >= AmoutRequire)
        {
            return true;
        }

        return false;
    }

    public void UnlockedTechPoint()
    {
        if (CanUnlockedTechPoint())
        {
            Technologies.CmdIncreaseTechPoint();
            
        }  
    }

    
    
    
    public bool UpdateInputs(GameObject go, GameObject parent, int nb)
    {
        parent.GetComponent<BuildingSlot>()?.AddItem(Utils.GetResourceButton(Utils.GetID(go)), nb);
        return true;
    }

    public void UpdateSlotInput()
    {
    }
    
    public override BuildingSlot GetSlot(int i)
    {
        if (i != 0)
            return null;
        return Inventory.GetInventory()[i].GetComponent<BuildingSlot>();
    }
}
