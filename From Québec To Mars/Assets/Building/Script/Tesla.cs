﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tesla : MonoBehaviour
{
    private float delay;
    private float range;
    private float nextattacktime;


    public void Start()
    {
        nextattacktime = Time.time;
        range = 12f;
        delay = 5f;
    }

    private void Update()
    {
        if (Time.time > nextattacktime)
        {
            GameObject[] monsteralpha = GameObject.FindGameObjectsWithTag("monsteralpha");
            GameObject[] monsterbeta = GameObject.FindGameObjectsWithTag("monsterbeta");
            for (int i = 0; i < monsteralpha.Length; i++)
            {
                if (Vector3.Distance(this.transform.position,monsteralpha[i].transform.position) <= range)
                {
                    monsteralpha[i].GetComponent<EnemyLife>().TakeDamage(20);
                }
            }
            for (int i = 0; i < monsterbeta.Length ; i++)
            {
                if (Vector3.Distance(this.transform.position,monsterbeta[i].transform.position) <= range)
                {
                    monsterbeta[i].GetComponent<EnemyLife>().TakeDamage(20);
                }
            }
            nextattacktime = Time.time + delay;
        }
    }

    
}
