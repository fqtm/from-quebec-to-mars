﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using MAP.Scripts;
using Mirror;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Drilling_Machine : Building
{
    [Header("Slots")]
    public BuildingSlot[] outputs;
    public BuildingSlot[] inputs;
    [Header("Other")]
    public float drillingTime;
    public Slider slider;
    [NonSerialized] [SyncVar] public float currentTime;
    private List<Tile.OreNature> nodes;
    private int length;
    private ID id;
    private bool started;
    

    public void Initiate()
    {
        ((Building) this).Awake();
        ((Building)this).Start();
        slider.minValue = -drillingTime;
        currentTime = drillingTime;
        id = GameObject.FindGameObjectWithTag("ID").GetComponent<ID>();
        SetNodes();
        SetInputs();
        started = true;
    }
    
    

    // Update is called once per frame
    void Update()
    {
        if (!started) return;
        transform.GetChild(1).gameObject.SetActive(true);
        currentTime -= Time.deltaTime;
        slider.value = -currentTime;
        if (currentTime < 0)
        {
            currentTime = drillingTime;
            int i = Random.Range(0, length);
            int j = GetOuput(nodes[i]);
            if (outputs[j].GetNb() == 0)
            {
                Instantiate(id.resources[(int) nodes[i]].GetComponent<Pickup>().itemButton, outputs[j].transform,
                    false);
            }
            IncreaseOutputCount(1, j);
        }
    }
    
    public void IncreaseOutputCount(int nb, int i)
    {
        if (!ClientScene.localPlayer.isServer)
            return;
        BuildingSlot slot = outputs[i];
        if (slot.GetNb() == 0)
            slot.UpdateSlot(nb);
        else
            slot.IncreaseNb(nb);
    }

    public void SetInputs()
    {
        int i = 0;
        List<Tile.OreNature> l = new List<Tile.OreNature>();
        foreach (Tile.OreNature node in nodes)
        {
            if (!l.Contains(node))
            {
                l.Add(node);
                Instantiate(id.resources[(int) node].GetComponent<Pickup>().itemButton, inputs[i].transform, false).GetComponent<DragNDrop>().enabled = false;
                i++;
            }
        }
    }

    public int GetOuput(Tile.OreNature ore)
    {
        for (int i = 0; i < 3; i++)
        {
            BuildingSlot slot = inputs[i];
            if (slot.transform.childCount == 0) break;
            GameObject button = slot.transform.GetChild(0).gameObject;
            uint id = button.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId();
            if (id == (int) ore)
                return i;
        }

        return -1; //Never theoretically happens
    }

    public void SetNodes()
    {
        nodes = new List<Tile.OreNature>();
        GridMap gridMap = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>().gridMap;
        gridMap.GetXY(transform.position, out int x, out int y);
        Tile.OreNature[,] map = gridMap.ores;
        
        for (int i = x; i < x + 2; i++)
        {
            for (int j = y; j < y + 2; j++)
            {
                if (map[j, i] != Tile.OreNature.None)
                {
                    nodes.Add(map[j, i]);
                }
            }
        }

        length = nodes.Count;
    }

    public void Load(string[] data)
    {
        currentTime = float.Parse(data[1]);
        for (int i = 0; i < 3; i++)
        {
            BuildingSlot slot = outputs[i];
            string[] couple = data[i + 4].Split(',');
            (uint id, int n) = (uint.Parse(couple[0]), int.Parse(couple[1]));
            if (n > 0)
            {
                Instantiate(Utils.GetResourceButton(id), slot.transform, false);
                slot.UpdateSlotAfterCall(n);
            }
        }
    }

    public override BuildingSlot GetSlot(int i)
    {
        return i >= 0 && i < 3 ? outputs[i] : null;
    }

    public GameObject PushObject()
    {
        if (outputs[0].go == null && outputs[1].go == null && outputs[2].go == null)
            return null;
        while (true)
        {
            int i = Random.Range(0, length);
            if (outputs[i].go != null)
            {
                GameObject ret = Utils.GetResource(Utils.GetID(outputs[i].go));
                if (outputs[i].nb == 1)
                {
                    outputs[i].Clear();
                }
                else if(ClientScene.localPlayer.isServer)
                {
                    outputs[i].IncreaseNb(-1);
                }

                return ret;
            }
        }
    }
}
