﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildingSlot : MonoBehaviour, IDropHandler, ISlotable
{
    public GameObject go;
    public int nb;
    public Text text;
    private BuildingNetwork BuildingNetwork;
    public int index;

    private void Start()
    {
        BuildingNetwork = ClientScene.localPlayer.GetComponent<BuildingNetwork>();
        go = null;
    }

    public void IncreaseNb(int i)
    {
        BuildingNetwork.CmdIncreaseSlotNb(transform.root.gameObject, index, i);
    }

    public void IncreaseNbAfterCall(int i)
    {
        if (CanAdd(i))
        {
            nb += i;
            if (nb < 2)
            {
                if (nb < 0) 
                    nb = 0;
                text.text = "";
            }
            else
            {
                text.text = nb.ToString();
            }
        }
    }

    // ReSharper disable Unity.PerformanceAnalysis
    public bool AddItem(GameObject Item, int n)
    {
        if (go == null)
        {
            Instantiate(Item, transform, false);
            UpdateSlot(n);
            return true;

        }

        if (go.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId() == Item.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId())
        {
            IncreaseNb(n);
            return true;
        }

        return false;
    }

    public void RemoveItem(int n)
    {
        if (nb > n)
        {
            IncreaseNb(-n);
            text = go.GetComponentInChildren<Text>();
        }
        else
        {
            Destroy(go);
            go = null;
        }
    }

    public bool CanAdd(int i)
    {
        return nb + i <= Utils.MaxStack;
    }
    
    public void OnDrop(PointerEventData eventData)
    {
        GameObject button = eventData.pointerDrag;
        ISlotable PrevSlot = button.GetComponentInParent<ISlotable>();
        ISlotable CurrentSlot = GetComponent<ISlotable>();
        if (button != null)
        {
            if (PrevSlot != CurrentSlot)
            {
                if (eventData.button == PointerEventData.InputButton.Left || PrevSlot.GetNb() == 1)
                {
                    ICatchable iCatchable = GetComponentInParent<ICatchable>();
                    if (iCatchable == null)
                        return;
                    if (iCatchable.UpdateInputs(button, gameObject, PrevSlot.GetNb()))
                    {
                        PrevSlot.Clear();
                        Destroy(button);
                    }
                }
                else if (eventData.button == PointerEventData.InputButton.Right)
                {
                    int Base = PrevSlot.GetNb();
                    int nb = Base % 2 == 0 ? Base / 2 : Base / 2 + 1;
                    if (PrevSlot != CurrentSlot)
                    {
                        ICatchable iCatchable = GetComponentInParent<ICatchable>();
                        if (iCatchable == null)
                            return;
                        if (iCatchable.UpdateInputs(button, gameObject, nb))
                        {
                            button.transform.SetParent(null);
                            Destroy(button);
                            PrevSlot.UpdateSlot(Base - nb);
                        }
                    }
                }
            }
        }
    }

    public void ClearAfterCall()
    {
        if (transform.childCount > 0)
        {
            GameObject button = transform.GetChild(0).gameObject;
            button.transform.SetParent(null);
            Destroy(button);
        }
        go = null;
        nb = 0;
        text = null;
    }

    public void Clear()
    {
        BuildingNetwork.CmdClearSlot(transform.root.gameObject, index);
    }

    public void UpdateSlot(int nb)
    {
        BuildingNetwork.CmdUpdateSlot(transform.root.gameObject, index, Utils.GetID(transform.GetChild(0).gameObject), nb);
    }

    public void UpdateSlotAfterCall(int nb)
    {
        go = transform.GetChild(0).gameObject;
        text = go.GetComponentInChildren<Text>();
        this.nb = 0;
        IncreaseNbAfterCall(nb);
        ICatchable iCatchable = GetComponentInParent<ICatchable>();
        iCatchable?.UpdateSlotInput();
    }

    public int GetNb()
    {
        return nb;
    }
}
