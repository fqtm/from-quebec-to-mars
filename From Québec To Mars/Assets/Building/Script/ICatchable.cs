using UnityEngine;


public interface ICatchable
{ 
    bool UpdateInputs(GameObject go, GameObject parent, int nb);

    void UpdateSlotInput();
}
