﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building_inventory : MonoBehaviour
{
    [SerializeField]
    private BuildingSlot[] inventory;

    public BuildingSlot[] GetInventory()
    {
        return inventory;
    }


    public bool ContainItem(GameObject Item)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i].go.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId() == Item.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId())
            {
                return true;
            }
        }

        return false;
    }

    public int ItemCount(GameObject Item)
    {
        int n = 0;
        for (int i = 0; i < inventory.Length; i++)
        {
            if (inventory[i].go != null)
            {
                if (inventory[i].go.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId() == Item.GetComponent<Spawn>().item.GetComponent<Pickup>().GetId())
                {
                    n += inventory[i].nb;
                }
            }
            
            
        }

        return n;
    }

    public bool RemoveItem(GameObject Item, int n)
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            if (Utils.GetID(inventory[i].go) == Utils.GetID(Item))
            {
                inventory[i].RemoveItem(n);
                return true;
            }
        }

        return false;
    }
    

    public bool IsFull()
    {
        for (int i = 0; i < inventory.Length ; i++)
        {
            if (inventory[i].go == null)
            {
                return false;
            }
        }

        return true;
    }
    
    

}
