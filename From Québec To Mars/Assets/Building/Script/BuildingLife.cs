﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class BuildingLife : NetworkBehaviour
{ 
    [SyncVar (hook = "SetHealth")] 
    public int health;
    public int maxHealth;
    public Slider slider;
    public Color Low;
    public Color High;
    public GameObject Canvas;
    private bool isDestroy;

    void Start()
    {
        CreateHealthBar();
    }

    public void CreateHealthBar()
    {
        slider.value = maxHealth;
        slider.maxValue = maxHealth;
    }

    public void SetHealth(int oldvalue, int newvalue)
    {
        slider.value = newvalue;
    }
    
    public void TakeDamage(int amount)
    {
        if(!isServer) return;
        health -= amount;
        if (health<=0) 
        {
            health = 0;
            isDestroy = true;
        }
    }

    public bool GetDestroy()
    {
        return isDestroy;
    }
    
    void Update()
    {
        Canvas.SetActive(health<maxHealth);
    }

}
