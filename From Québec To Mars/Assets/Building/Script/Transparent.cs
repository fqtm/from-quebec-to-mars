﻿using System;
using System.Collections;
using System.Collections.Generic;
using MAP.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class Transparent : MonoBehaviour
{
    
    private Vector3 mousePos;
    private SpriteRenderer sr;
    private Map map;
    [SerializeField] private GameObject building;
    private Building_Specifications bs;
    public bool isDrilling;


    private void Start()
    {
        map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
        sr = GetComponent<SpriteRenderer>();
        bs = building.GetComponent<Building_Specifications>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Utils.IsOverUI())
            sr.enabled = false;
        else
        {
            sr.enabled = true;
            map.gridMap.GetXY(Utils.GetMouseWorldPosition(), out int x, out int y);
            sr.sortingOrder = map.gridMap.GetHeight() - y;
            if (map.gridMap.IsEmpty(x, y, x + bs.GetWidth(), y + bs.GetHeight()) && (!isDrilling || map.gridMap.IsOnOre(x, y, x + bs.GetWidth(), y + bs.GetHeight())))
            {
                sr.color = Color.green;
            }
            else
            {
                sr.color = Color.red;
            }
            transform.position = map.gridMap.GetWorldPosition(x, y);
        }
        
    }
}
