﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    [NonSerialized] public bool IsInteracting;
    public CanvasGroup Menu;

    public void Interact()
    {
        Utils.Display(Menu, ref IsInteracting);
        transform.root.GetComponent<SpriteRenderer>().color = IsInteracting ? Color.yellow : Color.white;
    }
}
