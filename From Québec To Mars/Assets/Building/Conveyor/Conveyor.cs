﻿using System;
using System.Collections;
using System.Collections.Generic;
using MAP.Scripts;
using Mirror;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    public enum Type
    {
        Up,
        Right,
        Down,
        Left,
    }
    
    private Map map;
    public int x;
    public int y;
    private float Forcemagn;
    private float Angle;
    public Type state;

    private float nexttimepull;

    private void Start()
    {
        map = GameObject.FindWithTag("Map").GetComponent<Map>();
        map.gridMap.GetXY(transform.position,out x,out y);
        Forcemagn = gameObject.GetComponent<AreaEffector2D>().forceMagnitude;
        Angle = gameObject.GetComponent<AreaEffector2D>().forceAngle;
        state = WhichType(Forcemagn, Angle);
        nexttimepull = Time.time;
    }

    private void Update()
    {
        if (Time.time > nexttimepull)
        {
            switch (state)
            {
                case Type.Up :
                    if (map.gridMap.GetBuilding_Bool()[y-1,x].Item1 != null && x!=1000 && y!=1000)
                    {
                        GameObject drop = map.gridMap.GetBuilding_Bool()[y-1,x].Item1.GetComponent<IPullable>()?.PushObject();
                        if (drop != null)
                        {
                            float timesup = 2f;
                            nexttimepull = Time.time + timesup;
                            ClientScene.localPlayer.GetComponent<BuildingNetwork>().CmdSpawnDrop(Utils.GetID(drop),transform.position+new Vector3(0.5f,0.5f,0));
                        }
                        
                    }
                    break;
                case Type.Right :
                    if (map.gridMap.GetBuilding_Bool()[y,x-1].Item1 != null && x!=1000 && y!=1000)
                    {
                        GameObject drop = map.gridMap.GetBuilding_Bool()[y,x-1].Item1.GetComponent<IPullable>()?.PushObject();
                        if (drop != null)
                        {
                            float timesup = 2f;
                            nexttimepull = Time.time + timesup;
                            ClientScene.localPlayer.GetComponent<BuildingNetwork>().CmdSpawnDrop(Utils.GetID(drop),transform.position+new Vector3(0.5f,0.5f,0));
                        }
                    }
                    break;
                case Type.Down :
                    if (map.gridMap.GetBuilding_Bool()[y+1,x].Item1 != null && x!=1000 && y!=1000)
                    {
                        GameObject drop = map.gridMap.GetBuilding_Bool()[y+1,x].Item1.GetComponent<IPullable>()?.PushObject();
                        if (drop != null)
                        {
                            float timesup = 2f;
                            nexttimepull = Time.time + timesup;
                            ClientScene.localPlayer.GetComponent<BuildingNetwork>().CmdSpawnDrop(Utils.GetID(drop),transform.position+new Vector3(0.5f,0.5f,0));
                        }
                        
                    }
                    break;
                case Type.Left :
                    if (map.gridMap.GetBuilding_Bool()[y,x+1].Item1 != null && x!=1000 && y!=1000 )
                    {
                        GameObject drop = map.gridMap.GetBuilding_Bool()[y,x+1].Item1.GetComponent<IPullable>()?.PushObject();
                        if (drop != null)
                        {
                            float timesup = 2f;
                            nexttimepull = Time.time + timesup;
                            ClientScene.localPlayer.GetComponent<BuildingNetwork>().CmdSpawnDrop(Utils.GetID(drop),transform.position+new Vector3(0.5f,0.5f,0));
                        }
                        
                    }
                    break;
            }
            
        }
    }

    public Type WhichType(float f1,float f2)
    {
        if (f1 == 10 && f2 == 0) return Type.Right;
        if (f1 == -10 && f2 == 90) return Type.Down;
        if (f1 == 10 && f2 == 90) return Type.Up;
        return Type.Left;
    }
}
