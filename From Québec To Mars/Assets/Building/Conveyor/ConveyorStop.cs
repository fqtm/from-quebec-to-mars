﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorStop : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent<AreaEffector2D>(out  _))   
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
