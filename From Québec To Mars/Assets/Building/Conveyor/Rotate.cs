﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rotate : MonoBehaviour
{
    [SerializeField] private Sprite[] _sprites;
    [SerializeField] private float[] forceangles;
    [SerializeField] private float[] forcemagnitudes;
    [SerializeField] GameObject prefab;
    [SerializeField] GameObject prefabtrans;

    private void Start()
    {
        prefabtrans.GetComponent<SpriteRenderer>().sprite = _sprites[i];
        prefab.GetComponent<SpriteRenderer>().sprite = _sprites[i];
        prefab.GetComponent<AreaEffector2D>().forceAngle = forceangles[i];
        prefab.GetComponent<AreaEffector2D>().forceMagnitude = forcemagnitudes[i];
    }

    private int i;

    public void OnRotate()
    {
        Debug.Log("rotate");
        i++;
        if (i > 3) i = 0;
        prefabtrans.GetComponent<SpriteRenderer>().sprite = _sprites[i];
        prefab.GetComponent<SpriteRenderer>().sprite = _sprites[i];
        prefab.GetComponent<AreaEffector2D>().forceAngle = forceangles[i];
        prefab.GetComponent<AreaEffector2D>().forceMagnitude = forcemagnitudes[i];
    }

    public Sprite[] GetSprites()
    {
        return _sprites;
    }

    public int GetIndex()
    {
        return i;
    }

    

}

