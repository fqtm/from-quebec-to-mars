using System;
using MAP.Scripts;
using Mirror;
using Pathfinding;
using Player.Scripts;
using UnityEngine;

public class SaveSystem : NetworkBehaviour
{
    public void SaveGame()
    {
        GameObject.FindGameObjectWithTag("Map").GetComponent<Map>().Save();
        ClientScene.localPlayer.GetComponent<TechTree_Manager>().Save();
        Debug.Log("Save 1");
        RpcSavePlayer();
    }

    [ClientRpc]
    public void RpcSavePlayer()
    {
        NetworkIdentity player = ClientScene.localPlayer;
        Debug.Log("Save isClient:" + !player.isServer);
        player.GetComponent<PlayerNetwork>().Save();
        player.GetComponent<Inventory>().Save();
    }

    public void Load()
    {
        GameObject.FindGameObjectWithTag("Map").GetComponent<Map>().Load();
        GameObject.FindGameObjectWithTag("Player").GetComponent<TechTree_Manager>().Load();
        NetworkIdentity player = ClientScene.localPlayer;
        player.GetComponent<PlayerNetworkedSavingSystem>().CmdLoadPlayer();
    }
}
