﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Information : MonoBehaviour
{

    private CanvasGroup canvasGroup;
    private bool OnButton;
    public Text Text;
    private float delay;
    private void Start()
    {
         canvasGroup = GetComponent<CanvasGroup>();
         canvasGroup.alpha = 0;
         OnButton = false;
         delay = 1.5f;

    }

    public void ShowInformation(string text)
    {
        delay = 1.5f;
        Text.text = text;
        OnButton = true;
        

    }

    public void HideInformation()
    {
        canvasGroup.alpha = 0;
        OnButton = false;

    }

    public void Update()
    {
        if (OnButton)
        {
            delay -= Time.deltaTime;
            if (delay < 0)
            {
                transform.position = Input.mousePosition ;
                canvasGroup.alpha = 1;
            }
            
        }
        
    }
}
