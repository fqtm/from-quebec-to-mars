using System.Collections.Generic;
using MAP.Scripts;
using Mirror;
using UnityEngine;

public class ServerManager : NetworkManager
{
   public override void OnClientDisconnect(NetworkConnection conn)
    {
        NetworkIdentity player = ClientScene.localPlayer;
        if (player.isServer)
        {
            FindObjectOfType<SaveSystem>().SaveGame();
        }
        else
        {
            player.GetComponent<PlayerNetwork>().Save();
            player.GetComponent<Inventory>().Save();
        }
        
        player.GetComponent<TechTree_Manager>()._playersTech.unlockedTechTypeList =
            new List<Technologies.TechType>();
        StopClient();
    }
}
