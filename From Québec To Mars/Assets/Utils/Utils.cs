﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.EventSystems;

public class Utils 
{
    public static int MaxStack = 64;
    public static ID Id = GameObject.FindGameObjectWithTag("ID").GetComponent<ID>();

    public static Camera camera
    {
        get => GameObject.FindGameObjectWithTag("PlayerCamera").GetComponent<Camera>();
        set => camera = value;
    }
    public static Vector3 GetMouseWorldPosition()
    {
        Vector3 vec = GetMouseWorldPositionWithZ(Input.mousePosition, camera);
        vec.z = 0;
        return vec;
    }

    public static Vector3 GetMouseWorldPositionWithZ()
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, camera);
    }

    public static Vector3 GetMouseWorldPositionWithZ(Camera worldCamera)
    {
        return GetMouseWorldPositionWithZ(Input.mousePosition, worldCamera);
    }
    
    public static Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera)
    {
        return worldCamera.ScreenToWorldPoint(screenPosition);
    }
    
    //Create text in the world
    public static TextMesh CreateWorldText(string text, Transform parent = null, Vector3 localPosition = default(Vector3),
        int fontSize = 40, Color? color = null , TextAnchor textAnchor = TextAnchor.UpperLeft, TextAlignment textAlignment = TextAlignment.Center, int sortingOrder = 0)
    {
        if (color == null)
            color = Color.white;
        return CreateWorldText(parent, text, localPosition, fontSize, (Color)color, textAnchor, textAlignment, sortingOrder);
    }
    
    public static TextMesh CreateWorldText(Transform parent, string text, Vector3 localPosition, int fontSize,
        Color color, TextAnchor textAnchor, TextAlignment textAlignment, int sortingOrder)
    {
        GameObject gameObject = new GameObject("World_Text", typeof(TextMesh));
        Transform transform = gameObject.transform;
        transform.SetParent(parent, false);
        transform.localPosition = localPosition;
        TextMesh textMesh = gameObject.GetComponent<TextMesh>();
        textMesh.anchor = textAnchor;
        textMesh.alignment = textAlignment;
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.color = color;
        textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        return textMesh;
    }

    public static void Display(CanvasGroup canvasGroup, ref bool test)
    {
        if (test)
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
        }
        else
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts =  true;
        }

        test = !test;
    }

    public static GameObject GetResource(uint id)
    {
        if (id > Id.resources.Length)
            return null;
        return Id.resources[id];
    }

    public static GameObject GetResourceButton(uint id)
    {
        return GetResource(id).GetComponent<Pickup>().itemButton;
    }

    public static GameObject GetBuilding(uint id)
    {
        return Id.buildings[id];
    }

    public static uint GetID(GameObject go)
    {
        if (go == null) return UInt32.MaxValue;
        Pickup p = go.GetComponent<Pickup>();
        if (p != null)
            return p.GetId();
        Spawn s = go.GetComponent<Spawn>();
        if (s != null)
            return s.item.GetComponent<Pickup>().GetId();
        Building b = go.GetComponent<Building>();
        if (b is Furnace)
            return 0;
        if (b is Drilling_Machine)
            return 1;
        if (b is Assembly_machine)
            return 2;
        return UInt32.MaxValue;
    }

    public static CraftingRecipe GetRecipe(uint id)
    {
        return Id.recipes[id];
    }

    public static bool IsOverUI()
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = Input.mousePosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, raycastResults);
        for (int i = 0; i < raycastResults.Count; i++)
        {
            if (raycastResults[i].gameObject.GetComponent<MouseClickThrough>() != null)
            {
                raycastResults.RemoveAt(i);
                i--;
            }
        }

        return raycastResults.Count > 0;
    }
    
}
